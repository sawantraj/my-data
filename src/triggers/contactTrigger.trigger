trigger contactTrigger on Contact (after insert, after update,before delete){
    
    ContactHandler handler = new ContactHandler();
    
    If(trigger.isAfter){
        if(trigger.isInsert){
            handler.afterInsertContactHandlerMethod(trigger.newMap);  
        }
        
        if(trigger.isUpdate){
        	handler.afterUpdateContactHandlerMethod(trigger.newMap,trigger.oldMap); 
        }
    }
    
    If(trigger.isBefore){    
        if(trigger.isDelete){
        	handler.afterDeleteContactHandlerMethod(trigger.oldMap);
        } 
    }

}