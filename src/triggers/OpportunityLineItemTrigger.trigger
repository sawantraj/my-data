trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert) {
	
	ProductHandler handler= new ProductHandler();
	if(Trigger.isAfter){
		
		if(Trigger.isInsert){
			handler.afterInsert(Trigger.newMap); 
			
		}
	}

}