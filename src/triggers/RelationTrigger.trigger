trigger RelationTrigger on Relation__c (after insert,before delete) {
	
	RelationHandler handler = new RelationHandler();
	if(trigger.isAfter){
		
		if(trigger.isInsert){
			handler.afterInsertAmount(trigger.newMap);
		}
	}
	
	if(trigger.isBefore){
		
		if(trigger.isDelete){
			handler.afterDeleteAmount(trigger.oldMap);
		}
		
		system.debug('value of i' + RelationHandler.i); 
		
	}

}