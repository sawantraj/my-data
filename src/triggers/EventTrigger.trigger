/*
*   Trigger to insert  
*
*   Revision History:
*
*	Version		Author				Date			Description
*	1.0			Neena Tiwari		18/02/2013		Initial Draft
*
*/

trigger EventTrigger on Event (after insert) {
	EventHandler handler = new EventHandler();
	if(Trigger.isInsert){
		
		if(Trigger.isAfter){
			//handler.afterInsertAppointment(Trigger.newMap);
		} 
	}
	
}