public with sharing class RelationHandler {
	
	public static Integer i;
	public void afterInsertAmount(Map<Id,Relation__c>pRelInMap){
		DisplayTotalValueAmountRelation.InsertTotalAmount(pRelInMap); 
	
	}
	
	public void afterDeleteAmount(Map<Id,Relation__c> pRelDelMap){
		DisplayTotalValueAmountRelation.deleteAmount(pRelDelMap);
	}
	
	public integer value(Integer passValue){ 
		
		i = passValue;
		return i;
	}

}