// create the custom controller extension class
    public class Contact_Searcher {
        // Since we are creating an extension to the account standard controller,
        // create an account object to hold the current account
        Account a;
       
        public Contact_Searcher(ApexPages.StandardController controller)
        {
            // Get the current account, and store it in the account object
            a = (Account) controller.getRecord();
        }
       
        public string searchValue
        {
            get
            {
                if(searchValue == null)
                    searchValue = '';
                return searchValue;
            }
            set;
        }
       
        public List<Contact> searchResults
        {
            get
            {
                return (List<Contact>)setController.getRecords();
            }
            set;
        }
       
        public Boolean renderTable
        {
            get
            {
                if(renderTable == null)
                    return false;
                return renderTable;
            }
            set;
        }
       
        public void Next()
        {
            setController.Next();
        }
       
        public void Previous()
        {
            setController.Previous();
        }
     
        public ApexPages.StandardSetController setController
        {
            get
            {
                if(searchValue == '')
                {
                    List<Contact> contactList = new List<Contact>();
                    return new ApexPages.StandardSetController(contactList);
                }
                else
                    return setController;
            }
            set;
        }
       
        public Boolean getHasNext()
        {
            if(setController==null)
                return false;
            return setcontroller.getHasNext();
        }
     
        public Boolean getHasPrevious()
        {
            if(setController==null)
                return false;       
            return setcontroller.getHasPrevious();
        }
       
        public String getPageRangeText()
        {
            if(setController == null)
                return '';
            else
            {
                Integer startP = ((setController.getPageNumber() - 1) * setController.getPageSize()) + 1;
                Integer endP;
                if(setController.getPageNumber()*setController.getPageSize() > setcontroller.getResultSize())
                    endP = setcontroller.getResultSize();
                else
                    endP = setController.getPageNumber()*setController.getPageSize();
                return 'Showing Records : ' + startP + ' - ' + endP + ' of total ' + setcontroller.getResultSize();
            }
        }
       
     
        public Boolean getShowPageRangeText()
        {
            if (setController==null)
                return false;       
            if(setcontroller.getResultSize() > 0)
                return true;
            else
                return false;
        }   
       
        public void searchContacts()
        {
            String finalSearchValue = '%' + searchValue + '%';
            List<Contact> contactList = new List<Contact>([select Id, Name, Title, Department, Phone, Email from Contact where Account.Id = :ApexPages.CurrentPage().getParameters().get('Id')
                        and Name like :finalSearchValue]);
            setController = new ApexPages.StandardSetController(contactList); 
            setController.setPageSize(3);
            renderTable = true;
        }
    }