public with sharing class ContactHandler {
	
	
	
	Public void afterInsertContactHandlerMethod(Map<Id,Contact> pConMap){
		InsertContactNameToAccount.afterInsertContact(pConMap);
	}
	
	Public void afterDeleteContactHandlerMethod(Map<Id,Contact> pConDeleteMap){
		InsertContactNameToAccount.afterDeleteContact(pConDeleteMap);
	}
	
	Public void afterUpdateContactHandlerMethod(Map<Id,Contact> pConUpdateMap,Map<Id,Contact> pConBefUpdateMap){
		InsertContactNameToAccount.afterUpdateContact(pConUpdateMap,pConBefUpdateMap);
	}
	
	
}