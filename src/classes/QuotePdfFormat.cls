public with sharing class QuotePdfFormat 
{
	public PricebookEntry[] pricebookentryvalues{get;set;}
	public QuoteLineItem[] quotelinefields{get;set;}
	public Quote[] quotesvalues{get;set;}
	
	
	 public QuotePdfFormat(ApexPages.StandardController controller)
	 { 
	 	quotesvalues=[Select Id From Quote where Id=:controller.getRecord().Id limit 1];
		system.debug('@@@@@@@@@@@@@@@@@@==========================================================>quotesvalues.Id'+quotesvalues);

		quotelinefields=[Select q.Id, q.QuoteId, q.PricebookEntryId,PriceBookEntry.Product2.Name, q.TotalPrice, 
					q.UnitPrice, q.Subtotal, q.Quantity,q.Discount , q.ListPrice From QuoteLineItem q  where QuoteId=: quotesvalues];	
 	 											
 	}
 	
}