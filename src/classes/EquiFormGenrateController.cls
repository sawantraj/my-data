/* 
 *  Apex controller class call from EquiFormGenrate to edit and delete and create new R&D departments.
 *  Parameters : theDepartment as instance of R_D_Department__c object
 *               renderedValue as Boolean to rendered section when appropriate
 *               pageLoadRecordsLst as list of object
 *               wrapperLst as list of wrapper class
 *               cursorNo as Integer
 *              
 *               
 *  Return list of records to show on page and also perform Edit and delete functionality.
 * 
 *  Revision History: 
 * 
 *  Version     Author          Date        Description 
 *  1.0         Neena Tiwari    12/3/2013   Initial Draft 
 */
public with sharing class EquiFormGenrateController {
    
    Public R_D_Department__c theDepartment{get;set;}
    Public Boolean renderedValue {get;set;}
    Public integer i=0;
    Public List<R_D_Department__c> pageLoadRecordsLst{get;set;}
    Public List<DepartmentWrapper> wrapperLst{get;set;}
    Public integer cursorNo {get;set;}
    Public String contextItem{get;set;}
    Private ApexPages.StandardSetController setCon;
     
    //Controller
    public EquiFormGenrateController(ApexPages.standardController con){
        
        wrapperLst = new List<DepartmentWrapper>();
        theDepartment = (R_D_Department__c)con.getRecord();
        pageLoadRecordsLst = new List<R_D_Department__c>();
        
        //gather data set
        this.setCon = new ApexPages.StandardSetController([Select Work_to_be_done__c,  
                                     User__c,
                                     RequestDate__c,
                                     Reason__c,Id,
                                     Department_Name__c,
                                     Completed_Date__c,
                                     Comments__c 
                              From R_D_Department__c]); 
        this.setCon.setpageNumber(1);
        this.setCon.setPageSize(2);
        
        /*for(sobject r : this.setCon.getRecords()){
                    system.debug('in for');
            R_D_Department__c rd = (R_D_Department__c)r;
            pageLoadRecordsLst.add(rd);
        }*/
        renderedValue= False;
         
        //initialize object instance  
        theDepartment = new R_D_Department__c();
        
        //passing values to wrapper list to show on page. 
        //for(R_D_Department__c rd : PageLoadRecordsLst)
            //wrapperLst.add(new DepartmentWrapper(rd,++i));
        
    }//controller ends
    
    /*
    *   return current page of groups
    */
    public List<DepartmentWrapper> getDepartments(){
 
         wrapperLst.clear();
         wrapperLst = new List<DepartmentWrapper>();
 
        for(sObject r : this.setCon.getRecords()){
            R_D_Department__c c = (R_D_Department__c)r;
  			system.debug('value of i..' + i);
            DepartmentWrapper wrap = new DepartmentWrapper(c,++i); 
  			wrapperLst.add(wrap);
        }
 	return wrapperLst;
 
    }
    
    //Open section when user click on new button
    Public void getNewEquiform(){
        renderedValue = True;
        theDepartment = new R_D_Department__c();
        
    }
    
    //save newly created record and edited record to database.
    Public PageReference saveNewEquiForm(){
        try{
            upsert theDepartment;   
            
            //After update or insert it redirect to page with the modified or created values.
            PageReference result=Page.EquiFormGenrate; 
            result.getParameters().put('id', theDepartment.id);
            result.setRedirect(true);
            return result;
        }
        catch(Exception e){
        }
        return null;
          
    }//saveNewEquiForm methods ends
    
    //close the section when user cli8ck on cancel button
    Public void cancelNewEquiForm(){
        renderedValue = False;
    }
    
    //close the page and redirect to home page of object
    Public PageReference onClose(){
        PageReference equiFormCancel = new PageReference('/a0S/o');
        equiFormCancel.setRedirect(true);
        return equiFormCancel; 
        
    }
    //Edit existing selected record to modify changes and save to database.
    Public void editExistingPage(){
        renderedValue = True;
        theDepartment = new R_D_Department__c();
        for(DepartmentWrapper depEdit :wrapperLst){
            if(depEdit.indexNo == cursorNo){
                theDepartment = depEdit.dep;
                break;
            }
        }
            
    }//editExistingPage method ends
    
    //Delete the record from database after selecting particular record.
    Public PageReference deleteExistingPage(){
        for(DepartmentWrapper depDelete:wrapperLst){
            if(depDelete.indexNo == cursorNo){
                theDepartment = depDelete.dep;
                break;
            } 
        }
        Delete theDepartment;
        PageReference equiFormCancel = new PageReference('/apex/equiformgenrate');
        equiFormCancel.setRedirect(true);
        return equiFormCancel;  
    }//deleteExistingPage method ends
    
    //Wrapper class to count number of times record inserted.
    Public class DepartmentWrapper{
        
        Public R_D_Department__c dep{get;set;}
        Public  Integer indexNo {get;set;}
        
        Public DepartmentWrapper(R_D_Department__c dep,integer indexNo){
            
            this.dep = dep;
            this.indexNo = indexNo;
        }
    }
    
    // adavance to next page
    Public void doNext(){
        if(this.setCon.getHasNext()){
        	 this.setCon.next();
        }
            
    }
    
    //advance to previous page
    
    Public void doPrevious(){
        if(this.setCon.getHasPrevious())
        this.setCon.previous();
    }
    
    //return whether previous page exists
    Public Boolean getHasPrevious(){
        return this.setCon.getHasPrevious();
    }
    
    //return whether next page exists
    Public Boolean getHasNext(){
        system.debug('getHasNext' + this.setCon.getHasNext());
        return this.setCon.getHasNext();
    }
    
    //return page Number
    Public Integer getPageNumber(){
         return this.setCon.getPageNumber();
    }
    
    /*
    *    return total pages
    */
    Public Integer getTotalPages(){
 
        Decimal totalSize = this.setCon.getResultSize();
        Decimal pageSize = this.setCon.getPageSize();
 
        Decimal pages = totalSize/pageSize;
 
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
    
    
}//EquiFormGenrateController ends