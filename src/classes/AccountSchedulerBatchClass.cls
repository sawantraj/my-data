global class AccountSchedulerBatchClass implements Database.Batchable<sObject>,Database.Stateful{
    	
	public List<Account> UpdatedAppoinmentStatusRecordsLst;
    public AccountSchedulerBatchClass(){} 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    	system.debug('inside start method');
   	    return Database.getQueryLocator([Select NumberOfEmployees, 
   	    										Last_Name__c, 
   	    										First_Name__c, 
   	    										Description, 
   	    										AccountNumber 
   	    								 From Account
   	    								 where NumberofLocations__c = 5]);
    }//end of start method
    
    global void execute(Database.BatchableContext BC, List<Account> pAppointmentLst){ 
    	system.debug('values of pAppointmentLst'+pAppointmentLst);
    	UpdatedAppoinmentStatusRecordsLst = new List<Account>();  
        try{ 
        for(Account App:pAppointmentLst){ 
        	
        	App.Description = 'Overdue';
        	UpdatedAppoinmentStatusRecordsLst.add(App);
        	
        }
        update UpdatedAppoinmentStatusRecordsLst;
        system.debug('values of UpdatedAppoinmentStatusRecordsLst'+UpdatedAppoinmentStatusRecordsLst);
        }
        catch(Exception e){}
        	
     }//end of execute method
        
     global void finish(Database.BatchableContext BC){
     	system.debug('inside finish method');
     	//Build the system time of now + 600 seconds to schedule the batch apex.
		Datetime sysTime = System.now();
		sysTime = sysTime.addSeconds(600);
		String chron_exp = ' ' + sysTime.second() + ' ' 
							   + sysTime.minute() + ' ' 
							   + sysTime.hour() +' ' 
							   + sysTime.day() +' ' 
							   + sysTime.month() + ' ? ' 
							   + sysTime.year();
							   
		system.debug(chron_exp);
		AccountSchedulerClass2 AccountSchedulersced = new AccountSchedulerClass2();
		System.schedule(AccountSchedulerClass2.JOBNAME + sysTime.getTime(),chron_exp, AccountSchedulersced); 
     }     
}