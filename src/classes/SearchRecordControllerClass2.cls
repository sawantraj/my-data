public with sharing class SearchRecordControllerClass2 
{
	String SearchText;
	
			List<Account> accounts;
			List<Contact> contacts;
			List<Lead> leads;
			List<Opportunity> opportunities;
						
			public String getSearchText() 
				{
				return searchText;
				}
			
			public void setSearchText(String s) 
				{
			searchText = s;
				}
				
				public List<Account> getResultsAcc() 
				{
				return accounts;
				
				}
				public List<Contact> getResultsCon() 
				{
				return contacts;
					
				}
				public List<Lead> getResultsLeadq() 
				{
				return leads;
				
				}
				public List<Opportunity> getResultsOppor() 
				{
				return opportunities;
				
				}
		
		public PageReference dosearch()
		{
			
			if(SearchText.length()>0)
		{
			try{
        	List<List<SObject>> searchList = [FIND :searchText IN Name FIELDS RETURNING Account (Name,Last_Name__c,First_Name__c), 
					Contact(Name,FirstName,LastName),Lead(Name,LastName,FirstName),Opportunity(Name,Last_Name__c,First_Name__c )];

      		if ((searchList[0].size()==0)&& (searchList[1].size()==0)&&(SearchList[2].size()==0)&&(SearchList[3].size()==0))
   			{
   				ApexPages.Message apexmsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input');
   			ApexPages.addMessage(apexmsg);  
   			}
      		accounts = ((List<Account>)searchList[0]);
		 	contacts = ((List<Contact>)searchList[1]);
			leads = ((List<Lead>)searchList[2]);
	 	 	opportunities = ((List<Opportunity>)searchList[3]);
			}
			catch(NullPointerException e)
			{}
		}
			
		else if(SearchText.length()==0)
		{ApexPages.Message apexmsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input');
				ApexPages.addMessage(apexmsg);
		}
																		// error handling... when special chars  searchbox 		
		else if(searchText.contains('&') || searchText.contains('*')|| searchText.contains('#')||searchText.contains('/')||
           searchText.contains('!')||searchText.contains(')')||searchText.contains('(')||searchText.contains('<')||
           searchText.contains('>')||searchText.contains('"')||searchText.contains('}')||searchText.contains('{'))
   			{
   				ApexPages.Message apexmsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid input');
      			ApexPages.addMessage(apexmsg);  
   			}
   			
	
		return null;
 
  		}
}