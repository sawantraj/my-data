public with sharing class NewTaskAmazonControllerClass
{
	
	AWSKey__c keyObj = null;
	public string key { get { return credentials.key; } private set; }
	public string secret { get { return keyObj.secret__c; } private set; }
	
	public AWSKeys credentials {get;set;}
 	private String AWSCredentialName = 'EsplAmazon';
 	public S3.AmazonS3 as3 { get; private set; }	
 	public AWS_S3_Object__c record{get;set;}
	
	ApexPages.StandardController con;
	public  NewTaskAmazonControllerClass(ApexPages.StandardController stdController) {
        this.con = stdController;
        system.debug('-------------con.getRecord--------------'+con.getRecord());
		try { 
		this.record = [select id,bucket_name__c,content_type__c, file_name__c, access__c 
					   from AWS_S3_Object__c  where id = :con.getRecord().id limit 1];
		} catch( Exception ee) { 
			this.record = new AWS_S3_Object__c(); 
		}
		 system.debug('-------------con.getRecord--------------'+record);
		//as3 = new S3.AmazonS3(credentials.key,credentials.secret);
    	}
	
	 public PageReference constructor(){
    	try{
			
			credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);
		
		}catch(AWSKeys.AWSKeysException AWSEx){
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);
			 //throw new AWSKeys.AWSKeysException(AWSEx);
		  	 //ApexPages.addMessage(AWSEx);    
		}	
    
       return null;	
    }
	

	
		public String bucketToList {get;set;}
    
			public List<SelectOption> getBucketOptions(){
			List<SelectOption> options ;
				try{
					system.debug('======in getBucketOptions====');
				Datetime now = Datetime.now();
				if(as3!=null){
					String strSignature = as3.signature('ListAllMyBuckets',Datetime.now());
				system.debug('======strSignature===='+strSignature);
				}
				
				S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(key,Datetime.now(),
														as3.signature('ListAllMyBuckets',Datetime.now()));//perform web service call to amazonS3 and
															  //retrives all the bucket in your aws account.
				system.debug('@@@@@@@@@@=allBuckets=@@@@@@@@@@@'+allBuckets);
			
				options = new List<SelectOption>();
	   	 	
	   	 		for(S3.ListAllMyBucketsEntry bucket:  allBuckets.Buckets.Bucket ){
	   	 		options.add(new SelectOption(bucket.Name,bucket.Name));	
	   	 		}
		   		return options;
	   			}catch (System.NullPointerException e) {
	 			 System.debug('caught exception in listallmybuckets');
		   		ApexPages.addMessages(e);
				}catch(Exception ex){
		   //System.debug(ex);
		  		 System.debug('=====Excepetion====='+ex.getMessage());
			}
				return options;
			}
			
			public pageReference getsave() 
			{
				con.save();
				 system.debug('============con====='+con);
				 pageReference awsHomePage=new pageReference('/a05/o');
				 awsHomePage.setRedirect(true); 
				  system.debug('============awsHomePage====='+awsHomePage);      
		 		 return awsHomePage;
			
				}

}