public with sharing class ProductHandler {
	
	public void afterInsert(Map<Id,OpportunityLineItem> pOpportunityLineItemMap){
		UpdateContractStartDate.UpdateContract(pOpportunityLineItemMap);
	}

}