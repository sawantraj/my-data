public with sharing class SearchViaJqueryController {
	
	public List<sobject> objectList {get; set;} //List to  retrieving records from database
	public string theQuery;
	public string searchText{get;set;}
	public List<string> getAllFields{get;set;}
	
	
	public void getFieldsRecords(){
		try{
		Map<String, Schema.Sobjecttype> globalDescribeMap = Schema.getGlobalDescribe();
		Schema.Sobjecttype objSFDCObject = globalDescribeMap.get(searchText); 
        Schema.Describesobjectresult result = objSFDCObject.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = result.fields.getMap();
        getAllFields = new List<string>();
        objectList = new List<Sobject>();
       
        for(String strField : fieldMap.keyset()){ 
        	 Schema.Describefieldresult fieldResult = fieldMap.get(strField).getDescribe();
             getAllFields.add(fieldResult.getName());
             getAllFields.sort();
    	}//for
    	
    	theQuery ='Select ';
     	for(string valuesOfFields :getAllFields){
 			if(theQuery=='Select ')
	 		theQuery += valuesOfFields;
	 		else
	 		theQuery+=',  '+valuesOfFields;
 		}//for
 		
     	theQuery = theQuery + ' From  ' + searchText;
     	objectList= Database.query(theQuery);
    	}//try
	    
	 	catch(Exception e){
 			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'User atleast select 1 field'));
 		}//catch
	}
	
}//SearchViaJqueryController ends