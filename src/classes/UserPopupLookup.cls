global with sharing class UserPopupLookup {
	
	public UserPopupLookup(){}
	public UserPopupLookup(ApexPages.StandardController controller){}

	/* Object used to populate reference to user */
	public UserLookup__c lookupObject{get;set;}

	@RemoteAction
	global static String getUserData(String recordId){
	
		List<User> user = [select FirstName, LastName, Title, CompanyName from User where Id = : recordId];
		String JSONString = JSON.serialize(user);
		system.debug('Json Strting' + JSONString);

		//Depends on your needs and way you want to format your result. Lets just hardcode the status value for now.
		return '{"Records":' +JSONString+', "error": "null", "status":"SUCCESS"}';
	}
}