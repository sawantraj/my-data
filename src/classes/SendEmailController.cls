public with sharing class SendEmailController {
	
public Boolean emailSent {get;set;}
public string SendTo{get;set;}
public List<String> sendToLst = new List<String>();

public SendEmailController(ApexPages.standardController stController){}

public void sendEmail(){

Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage();
sendToLst.add(sendTo);

//set the recipient
mailHandler.setToAddresses(sendToLst);

//set the reply email address
mailHandler.setReplyTo('neena3003@gmail.com');
 
//set the display name
mailHandler.setSenderDisplayName('Neena');

//set the subject
mailHandler.setSubject('Hello');

mailHandler.setHtmlBody('Testing attached file');

try
{
Messaging.sendEmail(new Messaging.Email[] {mailHandler});
emailSent = true;
}
catch(EmailException e)
{
System.debug(e.getMessage());
emailSent = false;
}

}
}