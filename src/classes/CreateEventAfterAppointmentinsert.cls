public with sharing class CreateEventAfterAppointmentinsert {
	
	Public Static Integer afterInsertAppointment(Map<Id,Event> pEventMap,Map<Id,Event>pEventNewMap){
		 Map<Id,Id> eventAppointmentId = new Map<Id,Id>();
		 Map<Id,Appointment__c> appointmentMap = new Map<Id,Appointment__c>();
		 Map<Id,Event> eventMap = new Map<Id,Event>();
 
		
		for(Event eve :pEventMap.values()){
			if(eve.WhatId != Null)
			eventAppointmentId.put(eve.WhatId,eve.Id);
		}
		
		if (eventAppointmentId!=Null) return -1;
		
		for(Appointment__c appointment :[select Id,Start_Date__c,Client__r.Name,Location__c,Duration__c
										 From Appointment__c
										 where Id IN :eventAppointmentId.keyset()]){
		 	
			if(appointment != null)
			appointmentMap.put(appointment.Id,appointment);								 	
	 	}
	 	
	 	if(appointmentMap !=Null) return -2;
		
		for(Event eve : [Select  Id, Subject, StartDateTime,Location,DurationInMinutes, Description 
						 From Event e
						 where WhatId IN :appointmentMap.keyset()]){
						 	
			 	for(Appointment__c appointment :appointmentMap.values()){
			 			if(appointmentMap != null){
			 				
			 				eve.Location = appointment.Location__c;
			 				eve.StartDateTime = appointment.Start_Date__c;
			 				eve.WhatId = appointment.Client__r.Name;
			 				eventMap.put(eve.Id,eve); 
			 			}
			 			
			 			if(eventMap !=Null) return -3;
			 	}
		}
		
		
		return -1;
		
}//CreateEventAfterAppointmentinsert ends
}