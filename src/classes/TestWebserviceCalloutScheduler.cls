global class TestWebserviceCalloutScheduler implements Schedulable{ 
	
	public static String CRONEXP ='0 3 20 * * ?';     
    public static String JOBNAME = 'update status field';
	
	global void execute(schedulableContext sc){
    FacebookFeeds feeds = new FacebookFeeds();
    FacebookFeeds.callout(); 
    Datetime sysTime = System.now();
	sysTime = sysTime.addSeconds(120); 
	String chron_exp = ' ' + sysTime.second() + ' ' 
							   + sysTime.minute() + ' ' 
							   + sysTime.hour() +' ' 
							   + sysTime.day() +' ' 
							   + sysTime.month() + ' ? ' 
							   + sysTime.year();
							   
	system.debug(chron_exp);
	AccountSchedulerClass2 AccountSchedulersced = new AccountSchedulerClass2();
	System.schedule(AccountSchedulerClass2.JOBNAME + sysTime.getTime(),chron_exp, AccountSchedulersced); 
    
    }//end of execute method 

}