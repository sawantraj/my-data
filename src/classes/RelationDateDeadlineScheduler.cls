global class RelationDateDeadlineScheduler {
	
	public static string CRONEXP = '0 48 11 * * ?';
	public static string JOBNAME = 'Deadline Over';
	
	global void execute(schedulableContext sc){
		SendEmailToUserSchedulerClass objClass = new SendEmailToUserSchedulerClass();
		Database.executeBatch(objClass);
	}
	
	

}