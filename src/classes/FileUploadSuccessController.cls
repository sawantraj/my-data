public with sharing class FileUploadSuccessController{ 

	/** Start - All variables */
	public String strCaseId = '';
	public String strCaseAttachId = '';
	public String strContentType = '';
	public String strBucketName = '' ;
	public String strFileName = '' ;
	public Boolean isSitePage = false ;
	public Boolean isAttachment = false ;
	/** End - All variables */
	
	/**
	* Constructor: Fetches all the URL values and assigned it to variables.
	* @param None.
	*/
	public FileUploadSuccessController()
	{
		//fetching the case Id from URL
		if(Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '')
			strCaseId = Apexpages.currentPage().getParameters().get('id');
		
		//fetching the Case Attachment Id from URL	
		if(Apexpages.currentPage().getParameters().get('attachId') != null && Apexpages.currentPage().getParameters().get('attachId') != '')
			strCaseAttachId = Apexpages.currentPage().getParameters().get('attachId');	
		
		//fetching the Content type of file that is uploaded to amazon from URL	
		if(Apexpages.currentPage().getParameters().get('type') != null && Apexpages.currentPage().getParameters().get('type') != '')	
			strContentType = Apexpages.currentPage().getParameters().get('type');
		
		//fetching the bucket name of amazon where file is uploaded to amazon from URL
		if(Apexpages.currentPage().getParameters().get('bucket') != null && Apexpages.currentPage().getParameters().get('bucket') != '')	
			strBucketName = Apexpages.currentPage().getParameters().get('bucket');
		
		//fetching the file name of file that is uploaded to amazon from URL
		if(Apexpages.currentPage().getParameters().get('FileName') != null && Apexpages.currentPage().getParameters().get('FileName') != '')	
			strFileName = Apexpages.currentPage().getParameters().get('FileName');	
		
		//fetching 'site' string value from URL which identifies whether file is uploaded from site or not
		if(Apexpages.currentPage().getParameters().get('site') != null && Apexpages.currentPage().getParameters().get('site') != '')
		{
			// Converting string value to boolean
			if(Apexpages.currentPage().getParameters().get('site') == 'true')
				isSitePage = true ;			
		}
		
		//checking whether attachment is uploaded in amazon or not
		if(Apexpages.currentPage().getParameters().get('attachment') != null && Apexpages.currentPage().getParameters().get('attachment') != '')
		{
			if(Apexpages.currentPage().getParameters().get('attachment') == 'true')
				isAttachment = true;
		}
	}
	
	/*
		@MethodName: actionOnLoad 
		@param None
		@Description: Called from page action method. Creates Case_Attachment records and redirect to Case detail page.
	*/
	public pageReference actionOnLoad()
	{
		try
		{
			String strBaseUrl = '';
			String strSiteBaseUrl = Site.getCurrentSiteUrl();
			String strUrl = string.valueOf(url.getSalesforceBaseUrl());
			//assigning base URL to strUrl 
			if(strUrl.contains('=') && strUrl.contains(']'))
				strBaseUrl = strUrl.subString(strUrl.indexOf('=')+1, strUrl.indexOf(']'));
			else
				strBaseUrl = strUrl	;
			List<Case_Attachment__c> lstAttachment = [Select Id, Link_To_Download__c from Case_Attachment__c where Id=: strCaseAttachId]	;
			// checking whether file is uploaded from site or org.  Based on that providing the URL for download
			
			//if(!Test.isRunningTest())
			lstAttachment[0].Link_To_Download__c=(isSitePage == true)?strSiteBaseUrl +'DisplayAmazonFile?BucName='+strBucketName +'&FileName=' + strFileName : strBaseUrl + '/apex/DisplayAmazonFile?BucName=' + strBucketName + '&FileName=' + strFileName;
			
			system.debug('??????????????????I have sucessfull come here???????????????????');
				
			if(lstAttachment.size() > 0)
				update lstAttachment[0];
			
			/* Based on URL redirecting page */
			if(isAttachment && isSitePage)
			{    
				system.debug('??????????????In redirecting??????????????????' );
				pageReference objpg = new pageReference(strSiteBaseUrl+'CaseDetail?id='+strCaseId+'&confirmationAttachment=true');	
				objpg.setRedirect(true);
				return objpg;
			}
			/* Based on URL redirecting page */
			if(isSitePage && !isAttachment)
			{    
				system.debug('??????????????In redirecting??????????????????' );
				pageReference objpg = new pageReference(strSiteBaseUrl+'CaseDetail?id='+strCaseId+'&displayConfirmation=true');	
				objpg.setRedirect(true);
				return objpg;
			}
			/* Based on URL redirecting page */
			else if(lstAttachment[0].Id != null)
			{
				system.debug('??????????????In redirecting??????????????????' );
				PageReference pg = new Pagereference('/'+strCaseId);
				pg.setRedirect(true);
				return pg;
			}
			return null;
		}
		catch(Exception e)
		{
			return null;
		}
	}

}