public with sharing class OpportunityProduct2PageClass 
{
	 Id oppId;
	 public OpportunityProduct2PageClass(ApexPages.StandardController con)
	{
	this.oppId=[select Id, OpportunityId from OpportunityLineItem where Id = :con.getRecord().Id limit 1].OpportunityId;
	
	}
	 public pageReference redirect(){
        return new PageReference('/apex/OpportunityProductRedirect?id=' + oppId);
    }

}