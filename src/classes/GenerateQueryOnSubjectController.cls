/* 
 *	Apex controller class to genrate query after selection of any object from drop down list
 *	Parameters : theQuery as String
 *				 SelectObject as string
 *				 ObjectList as list of String
 *				 SelectedFields as list of String
 *				 optionsForObjectLst as list of select option
 *				 optionsForFieldsLst as list of select option
 *				 
 *	Return table of fields when any user needs to see records based on genrated query.
 * 
 * 	Revision History: 
 * 
 *	Version		Author			Date		Description 
 *	1.0			Neena Tiwari	10/1/2013	Initial Draft 
 */
 
public with sharing class GenerateQueryOnSubjectController {
	
	public boolean renderedValue{get;set;}
	public boolean PagesNumberFlag{get;set;}
	public boolean RadioIndicatorFlag{get;set;}
	public Integer PageSize{get;set;}
	public string theQuery{get;set;} //String variable to hold dynamic query 
	
	//String variable holds value of radio button when user clicked on it.
	public String RadioButtonValue  { get {return RadioButtonValue;} 
									  set {RadioButtonValue=value;} }
									  
	//String variable holds value of selected object
	public String SelectObject{ get {return SelectObject;} 
								set {SelectObject=value;} }
	public Integer totalNoOfRec;
	public Integer PageNumber;
	public Integer noOfPages;
								
	//String list holds value of selected fields of particular selected object
	public list<String> SelectedFields{ get {return SelectedFields;} 
										set {SelectedFields=value;} }
										
	public list<sObject> ObjectList {get; set;} //List to  retrieving records from database
	
	/* Select option list having values of all objects 
	 * and fields of dynamically selected object.
	 */ 
	public list<SelectOption> optionsForObjectLst{get;set;}
	public list<SelectOption> optionsForFieldsLst{get;set;}
	
	//Controller
	public GenerateQueryOnSubjectController(ApexPages.standardController stController){
		renderedValue=true;
		RadioIndicatorFlag=true;
		PageSize=10;
		ObjectList=new List<sObject>(); 
		PagesNumberFlag=(ObjectList.size()<0?true:false);
	}
	
	/* Void method to make flag value true so that all 
	 * functionality should be shown after clicking of radio button.
	 */
	public void OnClickedAction(){
		RadioIndicatorFlag =(RadioButtonValue.equalsIgnoreCase('None')?false:true);
	}
	
	/* GetSobject method to get all objects from salesforce org and put all 
	 * objects in select option list
	 */
	public list<SelectOption> getSobjects(){
		map<string,Schema.Sobjecttype> sobjectMap = Schema.getGlobalDescribe();
   		optionsForObjectLst = new list<SelectOption>();
   		if(sobjectMap.size()>0){
   			for(String objectValue:sobjectMap.keyset()){
   				Schema.DescribeSObjectResult objD = sobjectMap.get(objectValue).getDescribe();
   				optionsForObjectLst.add(new SelectOption(objD.getName(),objD.getName()));
   				optionsForObjectLst.sort();
   			}//for 
   		}
   		return optionsForObjectLst;
	}//getSobjects ends
	
	/*Fetching selected object from drop-down list and retriving all record*/
	private static Schema.Sobjecttype getObjectToken(String SelectObject){
		Map<String, Schema.Sobjecttype> globalDescribeMap = Schema.getGlobalDescribe();
     	return globalDescribeMap.get(SelectObject); 
	}//getObjectToken ends
	
	/* Created QueryRequiredValues method to bring all fields of selcted object 
	 * from drop-down list
	 */
	public void QueryRequiredValues(){
		try{
			if(SelectObject=='--None--')
			renderedValue=true;
			else 
			renderedValue=false;
			Schema.Sobjecttype objSFDCObject = getObjectToken(SelectObject);
	        Schema.Describesobjectresult result = objSFDCObject.getDescribe();
	        Map<String, Schema.SObjectField> fieldMap = result.fields.getMap();
	        optionsForFieldsLst = new list<SelectOption>();
	        
	        for(String strField : fieldMap.keySet()){ 
	             Schema.Describefieldresult fieldResult = fieldMap.get(strField).getDescribe();
	             string value = fieldResult.getName();
	             optionsForFieldsLst.add(new SelectOption(fieldResult.getName(),fieldResult.getName()));
	             optionsForFieldsLst.sort();
        	}//for
		}//try
		catch(Exception e){
 			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'please provide correct object, None is not the object'));
 		}//catch
             
     }//QueryRequiredValues ends
     
     /*Created SelectedFieldsResult method to genrate dynamic query and retrive 
      * all records from database
      */
     public void SelectedFieldsResult(){
 	  try{
     	theQuery ='Select ';
     	for(string valuesOfFields :SelectedFields){
 			if(theQuery=='Select ')
	 		theQuery += valuesOfFields;
	 		else
	 		theQuery+=',  '+valuesOfFields;
 		}//for
 		
     	theQuery = theQuery + ' From  ' + SelectObject + ' limit '+ PageSize;
     	
     	ObjectList=Database.query(theQuery);
	    system.debug('---------values of objectList'+ObjectList);
 	 }//try
	    
	 catch(Exception e){
 			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'User atleast select 1 field'));
 	 }//catch
	}//SelectedFieldsResult ends
	
	public void PaginationOfRecords(){
		PageNumber = 0;
		totalNoOfRec=ObjectList.size();
		system.debug('-total No of records-'+totalNoOfRec);
        noOfPages = totalNoOfRec/PageSize;
        
        if (Math.mod(totalNoOfRec, PageSize) > 0)
            noOfPages++;
     }
	
}// GenerateQueryOnSubjectController class ends