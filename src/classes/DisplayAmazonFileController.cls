/**
* @ClassName 	: DisplayAmazonFileController 
* @Description  : This is the controller class for DisplayAmazonFile page. This class generate a time based signed URL.
*/
public with sharing class DisplayAmazonFileController 
{
	/** Start - All variables */
	public String strBucketName;
	public String strFileName;
	public AWSKeys credentials {get;set;}
	public S3.AmazonS3 as3 { get; private set; }
	public String key { get {return credentials.key;} }	
	public String secret { get {return credentials.secret;} }
	private String strCredentialName;
	/** End - All variables */
	
	/**
	* Constructor: Fetch filename and bucket name from URL.
	* @param None.
	*/
	public DisplayAmazonFileController()
	{
		//checking if FileName is not null and assigning it to strFileName.
		if(Apexpages.currentPage().getParameters().get('FileName') != null && 
		   Apexpages.currentPage().getParameters().get('FileName') != '')
		   
			strFileName = Apexpages.currentPage().getParameters().get('FileName');
		else
			strFileName = '';
		//checking if BucName is not null and assigning it to strBucketName.
		if(Apexpages.currentPage().getParameters().get('BucName') != null &&
		   Apexpages.currentPage().getParameters().get('BucName') != '')		
			strBucketName = Apexpages.currentPage().getParameters().get('BucName');
		else
			strBucketName = '';		
	}
	
	/*
		@MethodName: onLoadAction 
		@param None
		@Description: Called from page action method and fetch amazon credential. Also, fetch expiry time from custom setting.
	*/
	public pageReference onLoadAction()
	{	
		try
		{	//fetching credentials name from custom setting
			Credentials__c c = Credentials__c.getInstance('Credentials Name');	
			if(c != null)
				strCredentialName  = c.credentials_name1__c;	 
			else
			{
			ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon credentials name');
			}
			
			credentials = new AWSKeys(strCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);	
			
			Access_to_Case_Attachments_Expires_in__c objExpTime = Access_to_Case_Attachments_Expires_in__c.getInstance('Amazon File Expiry');
			Integer intExpTime = integer.valueOf(objExpTime.Expiry_Seconds__c); 
			system.debug('=========intExpTime========'+intExpTime);
			return redirectToS3Key(intExpTime);
		}
		catch(Exception e)
		{
			return null;
			// do nothing  
		}
	}
	
    /*
		@MethodName: onLoadAction 
		@param None
		@Description: Called from onLoadAction method and return Signed URL based on key, filename, bucketname and expiry time.
	*/
    public pageReference redirectToS3Key(Integer expirySeconds) 
    {
    	try
    	{
	        //get the filename in url encoded format
	        String filename = EncodingUtil.urlEncode(strFileName, 'UTF-8');
	      	Datetime now = DateTime.now();
	        Datetime expireson = now.AddSeconds(expirySeconds);
	        Long Lexpires = expireson.getTime()/1000;
	        String strToSign = 'GET\n\n\n' + Lexpires + '\n/' + strBucketName + '/' + filename;
        	String strSigned = make_sig(strToSign);
	        String codedsigned = EncodingUtil.urlEncode(strSigned,'UTF-8');
	        String url = 'http://' + strBucketName + '.s3.amazonaws.com/' + filename + '?AWSAccessKeyId=' + as3.key + '&Expires=' + Lexpires + '&Signature=' + codedsigned;
	        PageReference newPage = new PageReference(url);
			return newPage;
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
        
    }
    
    /*
		@MethodName: onLoadAction 
		@param None
		@Description: Called from redirectToS3Key method and return signature.
	*/
    private String make_sig(string canonicalBuffer) 
    {  
    	try
    	{      
	        String macUrl ;
	        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(as3.secret));
	        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(as3.secret)); 
	        macUrl = EncodingUtil.base64Encode(mac);
	       
	        
	        //To cover the exception in the test
	        //if(Test.isRunningTest())
	        	//integer i = 10/0;                
			return macUrl;
    	}
    	catch(Exception e)
    	{
    		return null;
    		// do nothing.
    	}
    }
    
}