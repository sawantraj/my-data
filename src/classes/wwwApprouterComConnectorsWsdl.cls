//Generated by wsdl2apex

public class wwwApprouterComConnectorsWsdl {
    public class Provide_ServicePort {
        public String endpoint_x = 'https://localhost:443/receive';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.approuter.com/connectors/wsdl', 'wwwApprouterComConnectorsWsdl', 'http://schemas.microsoft.com/2003/10/Serialization/', 'schemasMicrosoftCom200310Serializat', 'http://tempuri.org/', 'tempuriOrg', 'http://schemas.datacontract.org/2004/07/IIFCASalesForce', 'schemasDatacontractOrg200407Iifcasa', 'http://schemas.microsoft.com/2003/10/Serialization/Arrays', 'schemasMicrosoftCom200310Serializat', 'http://tempuri.org/Imports', 'tempuriOrgImports'};
        public schemasDatacontractOrg200407Iifcasa.Result Provide_Service(schemasDatacontractOrg200407Iifcasa.IFCAAccount Account) {
            tempuriOrg.AccountUpdate_element request_x = new tempuriOrg.AccountUpdate_element();
            tempuriOrg.AccountUpdateResponse_element response_x;
            request_x.Account = Account;
            Map<String, tempuriOrg.AccountUpdateResponse_element> response_map_x = new Map<String, tempuriOrg.AccountUpdateResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://tempuri.org/',
              'AccountUpdate',
              'http://tempuri.org/',
              'AccountUpdateResponse',
              'tempuriOrg.AccountUpdateResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.AccountUpdateResult;
        }
    }
}