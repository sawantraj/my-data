public with sharing class DynamicCreateAppointmentRecordController {
		
	Public List<WrapperRecord> lstWrapper {get;set;}
	Public List<SelectOption> options{get;set;}
	Public String SelectObject {get;set;}
	Public Appointment__c appObj {get;set;}
	Public Integer Count;
	Public Boolean showingRows {get;set;}
	Public Integer selectedNumber {get {return selectedNumber;} 
								   set {selectedNumber=value;}}
	
	Public Boolean renderedValue{get;set;}
	
	Public DynamicCreateAppointmentRecordController(ApexPages.standardController stController){
		renderedValue = false;
		showingRows = false;
		Count= 0;
		lstWrapper= new List<WrapperRecord>();
		appObj = new Appointment__c();
		options = new List<SelectOption>();
	}
	
	Public void addRows(){
		system.debug('inside addRow..');
		showingRows = true;
	} 
	
	Public  List<SelectOption> getPicklistValues(){
	 	Schema.DescribeFieldResult fieldResult = Appointment__c.Status__c.getDescribe();
	 	for (Schema.PicklistEntry a : fieldResult.getPicklistValues()){ 
         	options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }
      	return options;
   	}
   	
   	Public List<SelectOption> getNoOfRows() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('2','2'));
		options.add(new SelectOption('3','3'));
		options.add(new SelectOption('4','4')); 
		options.add(new SelectOption('5','5'));
		options.add(new SelectOption('6','6'));
		options.add(new SelectOption('7','7'));
		return options;
	}
	
	Public Class WrapperRecord{
  		Public String SelectObject {get;set;}
  		Public Appointment__c appObj1 {get;set;}
  		Public String recCount{get;set;}
  		Public WrapperRecord(Integer Count){
  			Integer i;
  			if(Count!=Null){
  				for(i=0;i<=Count;i++){
  					system.debug('count value....' + Count);
  					
  					//create a new Appointment
  					appObj1 = new Appointment__c ();
  				}
  			}
		}
 	}
 	
 	Public PageReference saveAppointment(){
   		try{
   			Insert appObj;
   		}
		catch(Exception e){} 
		return null;
	}
	
	Public void Submit(){
		system.debug('Selected Number.....'+ selectedNumber);
		renderedValue = true;
		Count = selectedNumber;
		WrapperRecord wrap = new WrapperRecord(Count);
		
		//add the record in Inner class lst
		lstWrapper.add(wrap);
		System.debug(' lstInner........' + lstWrapper); 
		
	}
	
	/*Public Void AddAnotherRow(){
		
	}*/
   	
   
}