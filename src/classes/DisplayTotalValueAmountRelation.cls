public with sharing class DisplayTotalValueAmountRelation {
	
	public static Integer passValue;
	public static integer InsertTotalAmount(Map<Id,Relation__c> pRelInMap){
		Map<Id,Id> PCIdMap = new Map<Id,Id>();
		
		Map<Id,Relation__c> childValues = new Map<Id,Relation__c>();
		Map<Id,Relation__c> parentValues = new Map<Id,Relation__c>();
		
		
		for(Relation__c re :pRelInMap.values()){
				if(re.Parent_Relation__c !=null){
					PCIdMap.put(re.Parent_Relation__c,re.Id);
					
				}
		}//1st 
		passvalue=5;
		RelationHandler handler = new RelationHandler();
		handler.value(passValue);
		//populate all parent records in which total amount will get updated,
		//i.e total amount = sum of all child values.
		
		for(Relation__c re1 : [select Name,Total_Amount__c,Amount__c,Id 
							   from Relation__c 
							   where Id 
							   IN :pRelInMap.keyset()]){
			childValues.put(re1.Id,re1);
		}//2nd 
		
		for(Relation__c re2 : [select Name,Total_Amount__c,Amount__c,Id,Parent_Relation__c 
							   from Relation__c 
							   where Id
							   IN :PCIdMap.keyset()]){
			 
			Relation__c cRelation;
			if(PCIdMap.containsKey(re2.Id))
			cRelation = childValues.get(PCIdMap.get(re2.Id));
			
			if(re2.Parent_Relation__c == null){
				if(re2.Total_Amount__c == null )
					re2.Total_Amount__c = re2.Amount__c+ cRelation.Amount__c;
				else
					re2.Total_Amount__c = re2.Total_Amount__c+cRelation.Amount__c;
			}
			else{
				if(re2.Total_Amount__c == null )
					re2.Total_Amount__c = re2.Amount__c+ cRelation.Amount__c;
				else
					re2.Total_Amount__c = re2.Total_Amount__c+cRelation.Amount__c;
			}
			if(re2.Total_Amount__c !=null)
				re2.Total_Amount__c = re2.Total_Amount__c+cRelation.Amount__c;
			else
				re2.Total_Amount__c = re2.Amount__c+ cRelation.Amount__c;
			
			parentValues.put(re2.Id,re2);
		}//3rd
		
		//Persist changes to the database
		try{
			update parentValues.values();
			return 999;
		}
		catch(DmlException dmlException){
			System.debug('****DmlException: ' + dmlException.getMessage());
			return -3;
		}
		catch(Exception genException){
			System.debug('****Exception: ' + genException.getMessage());
			return -4;
		}
	}//InsertTotalAmount ends
	
	public static Integer deleteAmount(Map<Id,Relation__c> pRelDelMap){
		Map<Id,Id> PCIdMap = new Map<Id,Id>();
		Map<Id,Relation__c> childValues = new Map<Id,Relation__c>();
		Map<Id,Relation__c> parentValues = new Map<Id,Relation__c>();
		
		for(Relation__c re :pRelDelMap.values()){
			if(re.Parent_Relation__c !=null)
				PCIdMap.put(re.Parent_Relation__c,re.Id);
		}//1st 
		
		for(Relation__c re2 : [select Name,Total_Amount__c,Amount__c,Id,Parent_Relation__c
							   from Relation__c 
							   where Id 
							   IN :pRelDelMap.keyset()]){
			childValues.put(re2.Id,re2);
		}//3rd 
		
		system.debug('Child Values........'+ childValues);
		
		//update parent total amount for deleted child, 
		//i.e deduce deleted amount of child from Parent Total Amount 
		for(Relation__c re1 : [select Name,Total_Amount__c,Amount__c,Id 
							   from Relation__c 
							   where Id
							   IN :PCIdMap.Keyset()]){
			Relation__c CRelation;
			if(PCIdMap.containsKey(re1.Id)){
			system.debug('inside if....');
			CRelation = childValues.get(PCIdMap.get(re1.Id));
			}
			if(re1.Total_Amount__c != null){
				re1.Total_Amount__c = re1.Total_Amount__c - CRelation.Amount__c;
			}
			parentValues.put(re1.Id,re1);			   	
	  	}//2nd for
		
		//Persist changes to the database
		try{
			update parentValues.values();
			return 999;
		}
		catch(DmlException dmlException){
			System.debug('****DmlException: ' + dmlException.getMessage());
			return -3;
		}
		catch(Exception genException){
			System.debug('****Exception: ' + genException.getMessage());
			return -4;
		}
		
		return -1;
	
	}//deleteAmount ends
}