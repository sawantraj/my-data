public with sharing class TestPaginationSearchController {
 
	List<StudentWrapper> categories {get;set;}
	public ApexPages.StandardSetController con{get; set;}
	public List<Student_course__c> stucourse = new List<Student_course__c>();
	public String SearchValue{get;set;}
	string s;
 	
 	// ApexPages.StandardSetController must be instantiated
    // for standard list controllers
    public ApexPages.StandardSetController setCon {
    	get {
        	SearchValue=s;
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
															[Select Id, Name 
					 										 FROM Student__c  
					 										 where Name 
					 										 like :'%'+SearchValue+'%' 
		 													 limit 100]));
            }
            return setCon;
        }
        set;
    }
    
    // Initialize setCon and return a list of records
    public List<Student__c> getStudents() {
        return (List<Student__c>) setCon.getRecords();
    }
    
    //wrapper class
 	public class StudentWrapper {			
    public Boolean checked{ get; set; }
    public Student__c stu { get; set;}
 
    public StudentWrapper(){
        stu = new Student__c();
        checked = false;
    }
 
    public StudentWrapper(Student__c c){
        stu = c;
        checked = false;
    }
	}//inner class ends
 	
 	 
	// returns a list of wrapper objects for the sObjects in the current page set
	public List<StudentWrapper> getCategories() {
		system.debug('inside categories......');
		categories = new List<StudentWrapper>();
		for (Student__c  studentObj : (List<Student__c >)con.getRecords()){
			system.debug('value of category'+studentObj);
			categories.add(new StudentWrapper(studentObj));
 
		}
		return null;
	}
 
	// displays the selected items
 	public PageReference processOnSave() {
 		for (StudentWrapper cw : categories) {
 			if (cw.checked){
 				Student_course__c stucourseobj = new Student_course__c();
 				stucourseobj.Course__c = ApexPages.currentPage().getParameters().get('Id');
 				stucourseobj.Student__c= cw.stu.Id;
				stucourse.add(stucourseobj);
 			}
			}
 				upsert stucourse;
 				return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));}
 	
 	// returns the PageReference of the original page.			
 	 public PageReference oncancel(){
	 return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
	 }

 	
 
	// indicates whether there are more records after the current page set.
	public Boolean hasNext {
		get {
			return con.getHasNext();
		}
		set;
	}
 
	// indicates whether there are more records before the current page set.
	public Boolean hasPrevious {
		get {
			return con.getHasPrevious();
		}
		set;
	}
 
	// returns the page number of the current page set
	public Integer pageNumber {
		get {
			return con.getPageNumber();
		}
		set;
	}
 
	// returns the first page of records
 	public void first() {
 		con.first();
 	}
 
 	// returns the last page of records
 	public void last() {
 		con.last();
 	}
 
 	// returns the previous page of records
 	public void previous() {
 		con.previous();
 	}
 
 	// returns the next page of records
 	public void next() {
 		con.next();
 	}
 
}