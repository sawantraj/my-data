public with sharing class IframeContentController {
	public String redirectUrl {get; set;}
	public List<Quote> quotesLst {get;set;}
	
	public IframeContentController(ApexPages.StandardSetController controller) {
		quotesLst = [select Name,
					 		Quote.Opportunity.Name,
					 		QuoteNumber,
					 		Status,
					 		TotalPrice,
					 		Email,
					 		Quote.Contact.Name
					 From Quote
					 where Id='0Q090000000NlvE'];
		 system.debug('quotesLst :' + quotesLst);
	}
	
	
	public void executeLogic() {
		//Execute logic here
		
		this.redirectUrl = '/500/o';
	}
}