public with sharing class Awsvfcontroller 
{
	AWSKey__c keyObj = null;
	public string key { get { return credentials.key; } private set; }
	public string secret { get { return keyObj.secret__c; } private set; }
	
	public AWSKeys credentials {get;set;}
 	private String AWSCredentialName = 'EsplAmazon';
 	public S3.AmazonS3 as3 { get; private set; }	//called inner class of s3
 	public String serverURL {get;set;}
 	
 
	
	private final AWS_S3_Object__c obj;
	public AWS_S3_Object__c record{get;set;}
	
	ApexPages.StandardController con;
	public  Awsvfcontroller (ApexPages.StandardController stdController) {
        this.con = stdController;
        system.debug('-------------con.getRecord--------------'+con.getRecord());
		try { 
		this.record = [select id,bucket_name__c,content_type__c, file_name__c, access__c 
					   from AWS_S3_Object__c where id = :con.getRecord().id limit 1];
		} catch( Exception ee) { 
			this.record = new 	AWS_S3_Object__c(); 
		}
		//as3 = new S3.AmazonS3(credentials.key,credentials.secret);
    	}
	
	
	public PageReference constructor(){
    	try{
			
			credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);
		
		}catch(AWSKeys.AWSKeysException AWSEx){
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);
			 //throw new AWSKeys.AWSKeysException(AWSEx);
		  	 //ApexPages.addMessage(AWSEx);    
		}	
    
       return null;	
    }
    

	datetime expire = system.now().addDays(1);
	String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
		expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';           
	      
	string policy { get {return 
		'{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
    	record.Bucket_Name__c +'" } ,{ "acl": "'+
    	record.Access__c +'" },'+
    //	'{"success_action_status": "201" },'+
    	'{"content-type":"'+record.Content_Type__c+'"},'+
    	'{"success_action_redirect": "https://'+serverurl+'/'+record.id+'"},' +
    	'["starts-with", "$key", ""] ]}'; 	} } 
    
    public String getPolicy() {
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    // tester
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }
    
    //method that will sign
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    }
    
    public String bucketToList {get;set;}
    
			public List<SelectOption> getBucketOptions(){
			List<SelectOption> options ;
				try{
				Datetime now = Datetime.now();
				String strSignature = as3.signature('ListAllMyBuckets',Datetime.now());
				system.debug('======strSignature===='+strSignature);
				S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(key,Datetime.now(),as3.signature('ListAllMyBuckets',Datetime.now()));//perform web service call to amazonS3 and
															  //retrives all the bucket in your aws account.
				system.debug('@@@@@@@@@@@@@@@@@@@@@'+allBuckets);
			
				options = new List<SelectOption>();
	   	 	
	   	 		for(S3.ListAllMyBucketsEntry bucket:  allBuckets.Buckets.Bucket ){
	   	 		options.add(new SelectOption(bucket.Name,bucket.Name));	
	   	 		}
		   		return options;
	   			}catch (System.NullPointerException e) {
	 			 System.debug('caught exception in listallmybuckets');
		   		ApexPages.addMessages(e);
				}catch(Exception ex){
		   //System.debug(ex);
		  		 System.debug('=====Excepetion====='+ex.getMessage());
			}
				return options;
			}
    
	
	public pageReference save1() {
		con.save();
		PageReference p = new PageReference('/apex/news3object2?id='+ con.getRecord().id );
		p.getParameters().put('urlParam',serverURL);
		p.setRedirect(true);
		return p;	
	}
	
	public pageReference page2onLoad(){
	   PageReference tempPageRef = constructor();
	   // Need to get the salesforce.com server from the URL
	   System.debug('serverURL: ' +  ApexPages.currentPage().getParameters().get('urlParam'));
	   serverURL = ApexPages.currentPage().getParameters().get('urlParam');
	   //System.debug('serverURL: ' + serverURL);
	   String urlDomain = serverURL.substring(serverURL.indexOf('://')+3,serverURL.indexOf('/services'));
	   System.debug('URL Domain: ' + urlDomain);
	   serverURL = urlDomain;
	   return null;	
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*public Awsvfcontroller(ApexPages.StandardController con)
 	{
  		controller=con;
	
		record=	[select id,bucket_name__c,content_type__c, 
		file_name__c, access__c from AWS_S3_Object__c where id = 'a059000000322PF'];
 	}
 	public AWS_S3_Object__c getrecord()
 	{
 		return record;
 	}*/