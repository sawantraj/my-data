global class AccountSchedulerClass2 implements Schedulable {
	 public static String JOBNAME = 'Call webservice again';
	 
	global void execute(schedulableContext sc){
    FacebookFeeds feeds = new FacebookFeeds();
    FacebookFeeds.callout();
    Datetime sysTime = System.now();
	sysTime = sysTime.addSeconds(120); 
	String chron_exp = ' ' + sysTime.second() + ' ' 
							   + sysTime.minute() + ' ' 
							   + sysTime.hour() +' ' 
							   + sysTime.day() +' ' 
							   + sysTime.month() + ' ? ' 
							   + sysTime.year();
							   
	system.debug(chron_exp);
	TestWebserviceCalloutScheduler test = new TestWebserviceCalloutScheduler();
	System.schedule(AccountSchedulerClass2.JOBNAME + sysTime.getTime(),chron_exp, test);  
    }//end of execute method 

}