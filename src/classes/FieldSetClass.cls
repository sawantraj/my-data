public class FieldSetClass {

    public Account merch { get; set; }
    
    public FieldSetClass(ApexPages.standardController stController) {
        this.merch = getMerchandise();
    }

    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.new_set.getFields();
    }

    private Account getMerchandise() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'CreatedDate FROM Account LIMIT 1';
        return Database.query(query);
    }
}