/* Controller class call from CoursePanel Vf page to create panels and save record for
*  corresponding courses object.
* 	
*  Revision History:
*
*  Version	Date		Author			Comments
*  1.0		10/1/2012   Neena Tiwari    Initial Draft
*
*/

public with sharing class CoursePanelController {
	
	Public String deletedCourseRow{get;set;}
	Public Integer count = 1;
	Public List<courseValueWrapper>	courseInnerClassLst{get;set;}
	Public List<Course__c> courseLst{get;set;}
	
	//Construtor to add coming records in course list
	Public CoursePanelController(ApexPages.StandardController stdController){ 
	deletedCourseRow = '0';
	courseLst = new List<Course__c>();
	courseInnerClassLst = new List<courseValueWrapper>();
	}
	
	//inner class to delete selected row for the course object
	Public class courseValueWrapper{
		Public Course__c courses{get;private set;}
		Public String recCount{get;set;}
		Public Boolean isSelected{get;set;}
		
		
		Public courseValueWrapper(Integer pIntCount){
			recCount=string.valueOf(pIntCount);
			courses=new Course__c();
		}
	}
	
	//Create a method to add courses name when user click on a addCourse link 
	Public void addCourse(){
		//call to the inner class constructor and pass count value
		courseValueWrapper objInnerClass = new courseValueWrapper(count);
		
		//add the record to the inner class list
	    courseInnerClassLst.add(objInnerClass);
		count = count+1;   
    }//addCourse ends
	
	//Create a method to remove courses name when user click on a removeCourse link 
	Public void removeCourse(){
		try{
			Integer value = Integer.valueOf(deletedCourseRow)-1;
			courseInnerClassLst.remove(value);
		 	count = count - 1;
       	}
        catch(Exception ex){
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error :'+ex.getMessage());
       		ApexPages.addMessage(myMsg);
      	}
    }//removeCourse ends
    
    Public PageReference getSelected(){
    	courseLst.clear();
    	for(courseValueWrapper courseInnerclass : courseInnerClassLst){
    		if(courseInnerclass.isSelected == true){
    			courseLst.add(courseInnerclass.courses);
    		}
    	}
    	return null;
    }
    
     Public PageReference saveSelectedCourses(){
    	PageReference CourseNewPage = new PageReference('/a0K/o'); 
    	insert courseLst;
    	CourseNewPage.setRedirect(true);
    	return CourseNewPage;
    }
}//CoursePanelController ends