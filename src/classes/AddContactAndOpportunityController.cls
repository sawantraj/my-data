global class AddContactAndOpportunityController {
	
	
	@RemoteAction
    global static List<String> findSObjects(string obj){
    	 List<String> allSobjectValue;
    		if (obj == null){
        		return null;
        	}
	        else{
        		map<string,Schema.Sobjecttype> sobjectMap = Schema.getGlobalDescribe();
        		allSobjectValue = new List<String>();
   				
   				if(sobjectMap.size()>0){
   					for(String objectValue:sobjectMap.keyset()){
   						if(objectValue.startsWith(obj)){
   								if(!sobjectMap.get(objectValue).getDescribe().customSetting){
   									Schema.DescribeSObjectResult objD = sobjectMap.get(objectValue).getDescribe();
   									allSobjectValue.add(objD.getName());
   									allSobjectValue.sort();
   								}
   						}
   						
   					}//for 
        		}//if
        		return allSobjectValue;
			}//else
			
	}//findSObjects
}