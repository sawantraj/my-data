global with sharing class FetchRecordsFromWebserviceController {
	 	
	 	Public String LOGIN_DOMAIN = 'www';
	 	Public List<string> strField;
	 	Public String theQuery;
	 	Public String SESSION_ID;
	 	Public string SERVER_URL;
	 	Public List<selectOption> fieldsOptions {get;set;}
	 	Public List<selectOption> childObject{get;set;}
	 	Public List<WrapperSobjects> wrapSobLst {get;set;}
	    Public String Password {get;set;} 
	    Public String username {get;set;}
	   	Public Static String errMsg {get;set;}
	 	Public String SelectObject {get;set;}
	 	Public String selectChildObject{get;set;}
	    Public String displayError {get;set;}
	    Public boolean renderedValue1 {get;set;}
	    Public boolean renderedValue2 {get;set;}
	    Public boolean renderedValue3 {get;set;}
	    Public boolean showMessage {get;set;}
	    Public List<SelectOption> options {get;set;}
	    Public List<String> SelectedFields {get;set;}
	    
		
	 	
	    Public FetchRecordsFromWebserviceController(){
	    	displayError = 'none';
	    	renderedValue1 = false;
	    	renderedValue2 = false;
	    	renderedValue3 = false;
	    	showMessage = false;
        	theQuery='';
        	fieldsOptions = new List<selectOption>();
        	childObject = new List<selectOption>();
        	wrapSobLst = new List<WrapperSobjects>(); 
        }
	    
	    Public void GenrateSessionID(){
	   		renderedValue1 = true;
	    }
	    
	    Public Class WrapperSobjects{
	    	Public String Sname;
	 		Public String Slabel;
	 		
	 		Public WrapperSobjects(){}
	    }
	    
	  	//select list of sobjects
	    Public  List<SelectOption> getPicklistValues(){
	    	options= new List<SelectOption>();
		 	options.add(new selectoption('Account','Account'));
		 	options.add(new selectoption('Lead','Lead'));
		 	options.add(new selectoption('Opportunity','Opportunity'));
		 	options.add(new selectoption('Contacts','Contacts'));
		 	options.add(new selectoption('case','Case'));
	      	return options;
   		}//getPicklistValues ends
   		
   		
   		Public void  FieldsValuesFromObject(){ 
   			errMsg = 'Some error occurred, please try again';
   			renderedValue2 = true;
   			WrapperSobjects wrap = new WrapperSobjects();
   			try{
				fieldsOptions.clear();
				// Login via SOAP/XML web service api
				HttpRequest request = new HttpRequest();
				request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/u/22.0');
				request.setMethod('POST');
				request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
				request.setHeader('SOAPAction', '""');
				
				
				/*not escaping username and password because we're setting those variables above
				*in other words, this line "trusts" the lines above
				*if username and password were sourced elsewhere, they'd need to be escaped below
				*/
				request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' +
								username+ '</username><password>' + Password+ '</password></login></Body></Envelope>');
				
				
				Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
			  							.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
			  							.getChildElement('loginResponse', 'urn:partner.soap.sforce.com')
			 							.getChildElement('result', 'urn:partner.soap.sforce.com');
	 				
 				// Grab session id and server url
				SERVER_URL = resultElmt.getChildElement('serverUrl', 'urn:partner.soap.sforce.com').getText().split('/services')[0];
				SESSION_ID = resultElmt.getChildElement('sessionId', 'urn:partner.soap.sforce.com').getText();
				
				final PageReference theUrl = new PageReference(SERVER_URL + '/services/data/v27.0/sobjects/'+SelectObject+'/describe');
				system.debug('the final url:'+ theUrl);
				request = new HttpRequest();
				request.setEndpoint(theUrl.getUrl());
				request.setMethod('GET');
				request.setHeader('Authorization', 'OAuth ' + SESSION_ID);
 				String body = (new Http()).send(request).getBody();
 				//System.debug(response.getBody());
				JSONParser parser = JSON.createParser(body);
  				do{
  					parser.nextToken();
  					system.debug('inside parser...');
  					if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText().tolowercase() =='name')){
  						parser.nextToken();
  						wrap.Sname = parser.getText();
  						fieldsOptions.add(new selectOption(wrap.Sname,wrap.Sname));
  					}	
  				}while(parser.hasCurrentToken());
  				
	  		}//try ends
			catch(Exception e){ 
				showMessage = true;
				system.debug('Exception Caught---------'+e);
				
				//ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error:Invalid Input.');
          		//ApexPages.addMessage(myMsg);
				displayError = 'block';
			}
		      
		}//getFieldsValuesFromObject ends
		
		
		Public void retriveRelatedListFromWebservice(){
			errMsg = 'Some error occurred, please try again';
			renderedValue3 = true;
			try{
				WrapperSobjects wrap = new WrapperSobjects();
				final PageReference theUrl = new PageReference(SERVER_URL + '/services/data/v27.0/sobjects/'+SelectObject+'/describe');
				HttpRequest request = new HttpRequest();
				request.setEndpoint(theUrl.getUrl());
				request.setMethod('GET');
				request.setHeader('Authorization', 'OAuth ' + SESSION_ID);
	 			String body = (new Http()).send(request).getBody();
	 			system.debug('Response Body---------'+body);
	 			JSONParser parser = JSON.createParser(body);
  				do{
  					parser.nextToken();
  					system.debug('current object....');
  					
  					if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText().tolowercase() =='childSObject')){
  						parser.nextToken();
  						wrap.Sname = parser.getText();
  						system.debug('WRAP value...'+ wrap.Sname);
  						childObject.add(new selectOption(wrap.Sname,wrap.Sname));
  						system.debug('value of childObject' + childObject);
  					}	
  				}while(parser.hasCurrentToken());
 			}//try ends
			catch(Exception e){}
			
		} 
}//FetchRecordsFromWebserviceController ends