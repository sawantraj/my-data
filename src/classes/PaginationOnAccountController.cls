public with sharing class PaginationOnAccountController {
	
	public List<Account> accLst{get;set;}
	private integer totalRecs = 0;     
    private integer index = 0;
    private integer blockSize = 10;   
	
	public PaginationOnAccountController(ApexPages.standardController stController){
		totalRecs =[select count() from Account];
		system.debug(' Total Rec : ' + totalRecs);
		
		
	} // constructor
	public List<Account> getAccount() 
    {
        List<Account> acc = Database.Query('SELECT Name, Id,NumberOfEmployees,Phone FROM Account LIMIT :blockSize OFFSET :index');
        System.debug('Values are ' + acc);
        return acc;
    }    
	
	public void beginning()
    {
        index = 0;
    }
    
    public void previous()
    {
        index = index - blockSize;
    }
    
    public void next()
    {
    	system.debug('inside next');
        index = index + blockSize;
    }

    public void end()
    {
    	system.debug('inside End');
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev()
    {
        if(index == 0)
        return true;
        else
        return false;
    }  
    
    public boolean getnxt()
    {
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    }         

}