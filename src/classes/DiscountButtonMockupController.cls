public with sharing class DiscountButtonMockupController 
{
	
	public OpportunityLineItem[] shoppingCart {get;set;}
	 public Opportunity theOpp {get;set;}
	
	
	 public DiscountButtonMockupController(ApexPages.StandardController controller)
	 { 
	 	theOpp = [select Id from Opportunity limit 1];
		

		 shoppingCart = [select Id, Quantity, TotalPrice, UnitPrice,ListPrice,Description, PriceBookEntryId, PriceBookEntry.Name, 
     					PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id
     					 from opportunityLineItem where OpportunityId=:theOpp.Id];			 
 	 }
 	 
}