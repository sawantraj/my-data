public with sharing class selectRadioTestController {
	
	public String Dates{get;set;}
	public Account acc;

	public selectRadioTestController(ApexPages.StandardController stdController){
		this.acc = (Account)stdController.getRecord();
		acc = [select SLAExpirationDate__c from Account where Id =:acc.Id];
	}

}