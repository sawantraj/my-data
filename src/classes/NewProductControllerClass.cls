public with sharing class NewProductControllerClass 
{
	private final Product2 products{get;set;}
	
	 public List<Product2> products1 {get; set;}
	
	public NewProductControllerClass(ApexPages.StandardController stdController)
	{

		this.products = (Product2)stdController.getRecord();
		products1=new List<Product2>();
		products1.add(new Product2());
	}
	 
	public void addrow()
	{
	 products1.add(new Product2());
		
	}
	 public void removeRow()
     {
        Integer i = products1.size();
         products1.remove(i-1);
       }
     
	public PageReference save() 
	{
		 insert products1;       
		 PageReference home = new PageReference('/home/home.jsp');       
		home.setRedirect(true);       
		 return home;
	}

		 
}