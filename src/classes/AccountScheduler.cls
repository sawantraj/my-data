global class AccountScheduler implements Schedulable {
    public static String CRONEXP ='0 39 2 * * ?';   
    public static String JOBNAME = 'update status field';
    
    global void execute(schedulableContext sc){
    AccountSchedulerBatchClass objScheduleAppointmentStatusUpdate = new AccountSchedulerBatchClass();
    Database.executeBatch(objScheduleAppointmentStatusUpdate);
    }//end of execute method 
  
 }//end of AppointmentSchduledClass