global class GoogleChartsController {
global static Account acc{get;set;}
 
    public GoogleChartsController(ApexPages.StandardController controller) {
        acc = (Account)controller.getRecord();
    }
@RemoteAction   
    global static AggregateResult[] loadChildCases(Id prmAccountId) {
        AggregateResult[] caseLst = [Select ParentId, count(Id) FROM Case  WHERE AccountId = :prmAccountId Group By ParentId ];
        
        return caseLst;
    }
    }