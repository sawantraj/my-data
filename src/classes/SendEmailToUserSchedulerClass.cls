global class SendEmailToUserSchedulerClass implements Database.Batchable<sobject>,Database.Stateful{
	
	public list<Relation__c> ValueToSendingEmail;
	
	public SendEmailToUserSchedulerClass(){}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
    	system.debug('inside start method');
   	    return Database.getQueryLocator([Select Amount__c,Deadline__c,Name
   	    								 From Relation__c
   	    								 where Deadline__c = today]);
    }//end of start method
    
     global void execute(Database.BatchableContext BC, List<Relation__c> pRelationLst){ 
    	/*system.debug('values of pRelationLst'+pRelationLst);
    	ValueToSendingEmail = new List<Relation__c>();  
        try{ 
        for(Relation__c rel:pRelationLst){ 
        	
        	App.Description = 'Overdue';
        	UpdatedAppoinmentStatusRecordsLst.add(App);
        	
        }
        update UpdatedAppoinmentStatusRecordsLst;
        system.debug('values of UpdatedAppoinmentStatusRecordsLst'+UpdatedAppoinmentStatusRecordsLst);
        }
        catch(Exception e){}*/
        	
     }//end of execute method
     
     global void finish(Database.BatchableContext BC){
     	
     	// Query the AsyncApexJob object to retrieve the current job's information.
   		AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
      							 TotalJobItems, CreatedBy.Email
      					  FROM AsyncApexJob 
      					  WHERE Id =:BC.getJobId()];
     	Messaging.Singleemailmessage mail = New Messaging.Singleemailmessage();
     	String [] toAddress = new string[] {a.CreatedBy.Email};
     	mail.setToAddresses(toAddress);
     	mail.setSubject('Deadline to submit Amount'+ a.Status);
     	mail.setPlainTextBody('The batch Apex job proceed' + a.TotalJobItems + 'batches with' + a.NumberOfErrors + 'failures.');
     	Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
     }// finish method ends  
        
} //SendEmailToUserSchedulerClass ends