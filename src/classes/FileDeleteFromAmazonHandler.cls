/*@Description  : This is the Handler class for FileToDeleteFromAmazon. 
				  This class deletes attachment from Amazon s3. 
*/
public class FileDeleteFromAmazonHandler {
	
	/** Start - All variables */
	public static String bucketNameToDelete {get;set;} 
	private static String AWSCredentialName; //Modify this string variable to be the name of the AWS Credential record that contains the proper AWS keys and secret
    public static S3.ListEntry[] bucketList {get;set;}    
  	public static  S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
    public static String deleteObjectErrorMsg {get;set;}
  	public static String objectToDelete {get;set;}
    public static String S3Key {get;set;}
    public static String bucketToList {get;set;}
    public static S3.AmazonS3 as3 { get; private set; } //This object represents an instance of the Amazon S3 toolkit and makes all the Web Service calls to AWS. 
	public static S3.ListBucketResult listbucket {get;set;}
	public static String selectedBucket {get;set;}
	/** End - All variables */
	
	/*
	@MethodName: onBeforeDelete 
	@Description: Called from Delete trigger and deletes Attachment__c records from Amazon.
	*/
	
	 @future (callout=true)
	public static void onBeforeDelete(List<String> s){
		    for(string value:s){
		    try{	
			system.debug('=========value under attachment---'+s);
			
			//using custom settings to get credentials Name
			
			Credentials__c c = Credentials__c.getInstance('Credentials Name');	
			if(c != null)
				AWSCredentialName  = c.credentials_name1__c;	 
			else
			{
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon credentials name');
				return;
			}
			AWSKeys credentials = new AWSKeys(AWSCredentialName);
			
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);//required key and secret key to connect from Amazon
			S3Key= credentials.key;
				
			//using custom settings to get Bucket Name
			AmazonBucket1__c b = AmazonBucket1__c.getInstance('Bucket Name1');
			if(b != null)
				bucketToList = b.Bucket_Name__c;	 
			else
			{
				bucketToList='esp4';
			}
				
				
		   	objectToDelete=value;
				
			Datetime now = Datetime.now();        
			//This performs the Web Service call to Amazon S3 and delete file from bucket.
		   S3.Status deleteObjectReslt= as3.DeleteObject(bucketToList,objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
	       System.debug('----------Successfully complete S3.status------: ');
	       }
		    
       catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
	   }
       catch(Exception ex){
		   System.debug(ex);
		 }
		    }
		
	  }}