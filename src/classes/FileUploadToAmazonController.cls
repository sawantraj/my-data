/*@Description  : This is the controller class for UploadFileAmazon page. 
				  This class Upload File Attachment to send in Amazon and redirect to Object detail page. 
*/
public with sharing class FileUploadToAmazonController 
{
/** Start - All variables */
    public String strFileName {get;set;}	
	public String strBucketName {get;set;}		
	public AWSKeys credentials {get;set;}	 
	public S3.AmazonS3 as3 { get; private set; } 	
	public Attachment__c objAttachment {get;set;}	  
	private String AWSCredentialName;	
	public String secret { get {return credentials.secret;} }	
	public String key { get {return credentials.key;} }	
	public String strfileContentType{get;set;}	 
	public String strformattedexpire{get;set;}	
	public String strId{get;set;}	
	public String strObjName{get;set;}
	public String strAttachmentId{get;set;}	
	public String strBaseUrl {get;set;}	
	public String strsite{get;set;}
	/** End - All variables */
	
	/* Start : variables used for signing the file being uploaded */

	Datetime expire = system.now().addDays(1);
	
	String formattedexpire = strformattedexpire = expire.formatGmt('yyyy-MM-dd') + 'T' +   
	expire.formatGmt('HH:mm:ss') + '.' + expire.formatGMT('SSS') + 'Z';  
        
	      
	String policy { 
					get 	
					{
						return 	'{ "expiration": "'+ formattedexpire +'","conditions": [ {"bucket": "'+  
			    		strBucketName +'" } ,{ "acl": "private" },'+
				        '{"content-type":"'+	+'"},'+
				     	'{"success_action_redirect": "'+ strBaseUrl +'/apex/SuccessFileUpload?id=' + strId + '&type=' + strfileContentType + '&FileName=' + strFileName + '&attachId=' + strAttachmentId + '&site=' +strSite + '"}, ' +
				    	'["starts-with", "$key", "' + strFileName + '"] ]}'; 	
				   }
			     } 
    	 	
    /* End : variables used for signing the file being uploaded */
    
    /**
	* Constructor: Fetches the Object id from url and initialize all the global variables.
	* @param : Apexpages.StandardSetcontroller of Attachments__c.
	*/
    public FileUploadToAmazonController(Apexpages.StandardSetcontroller sc) {		
		Initialize(); //  Initialize all the global variables
		datetime expireDate = system.now().addDays(1);
		strformattedexpire = expireDate.formatGmt('yyyy-MM-dd') + 'T' +	expireDate.formatGmt('HH:mm:ss') + '.' + expireDate.formatGMT('SSS') + 'Z';  
		strId = ApexPages.currentPage().getParameters().get('id');
		strObjName = ApexPages.currentPage().getParameters().get('Obj');
		// fetch the base URL of current org.
		String strUrl = string.valueOf(url.getSalesforceBaseUrl());
		if(strUrl.contains('=') && strUrl.contains(']'))
 			strBaseUrl = strUrl.subString(strUrl.indexOf('=')+1, strUrl.indexOf(']'));
 		else
 			strBaseUrl = strUrl;	
		strSite='apex/DisplayAmazonFile';
		}
	/*
		@MethodName: Initialize 
		@param None
		@Description: Initialize all the global variables
	*/
	public void Initialize() {
    	strFileName = '';
		strBucketName = '';
		strfileContentType = '';
	}
	
	/*
		@MethodName: getPolicy 
		@param None
		@Description: create and encode the policy.
	*/
	public String getPolicy() {
		System.debug('---------------formattedexpire----------'+formattedexpire);
		System.debug('---------------strBucketName----------'+strBucketName);
		System.debug('---------------strfileContentType----------'+strfileContentType);
		System.debug('---------------strBaseUrl----------'+strBaseUrl);
		System.debug('---------------strId---------'+strId);
		System.debug('---------------strFileName----------'+strFileName);
		System.debug('---------------strAttachmentId----------'+strAttachmentId);
		System.debug('---------------key----------'+key);
		System.debug('---------------secret----------'+secret);
		System.debug('---------------strBaseUrl----------'+strBaseUrl);
    	return EncodingUtil.base64Encode(Blob.valueOf(policy));
    	}  
	
	/*
		@MethodName: OnLoadAction 
		@param None
		@Description: fetch the key and secret ket of amazon org and fetch the bucket name from custom setting.
	*/
	public void actionOnLoad()
	{
		try{
			//fetching Credentials name from custom setting
			Credentials__c c = Credentials__c.getInstance('Credentials Name');	
			if(c != null)
				AWSCredentialName  = c.credentials_name1__c;	 
			else
			{
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon credentials name');
				return;
			}
			
			//fetching Amazon org secret key and key  
			credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);	
			
			//fetching bucket name from custom setting	
			AmazonBucket1__c b = AmazonBucket1__c.getInstance('Bucket Name1');	
			if(b != null)
				strBucketName = b.Bucket_Name__c;	 
			else{
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon bucket name');
				
				return;
			}
		}
		catch(AWSKeys.AWSKeysException AWSEx) {
		     System.debug('Caught exception in FileUploadToAmazonController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);			
		}	
		return;	
	}
	
	/*
		@MethodName: getSignedPolicy 
		@param None
		@Description: form the signature for form post.
	*/	   
    public String getSignedPolicy()  {   
    	
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    } 
    
    /*
		@MethodName: make_sig 
		@param None
		@Description: return the mac url for the signature created.
	*/
    private String make_sig(string canonicalBuffer) { 
    	try {       
	        String macUrl ;
	        String signingKey = EncodingUtil.base64Encode(Blob.valueOf('J1Qi6S8xvAxdMEksUJJ/mjBK+9SFAGTgHHzf2uc9'));
	        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof('J1Qi6S8xvAxdMEksUJJ/mjBK+9SFAGTgHHzf2uc9')); 
	        macUrl = EncodingUtil.base64Encode(mac); 
	        
	        //To cover the exception in the test
				/*if(Test.isRunningTest())
					integer i = 10/0; */
				 return macUrl;
    	}
    	catch(Exception e) {
    		return null;
    		//Do Nothing.
    	}
    }
    
	/*
		@MethodName: fetchRelatedContentType 
		@param None
		@Description: dummy function called on selection of file and rerender the form post.
	*/
	public void dummyFunction() {
		//Do nothing
    }
    
    /*
		@MethodName: submitAttachment 
		@param None
		@Description: function called on click of Submit'. It inserts Attachment record
	*/
	
    public void submitAttachment()  {
    	try {
    		
	    	List<Attachment__c> lstAttachment = new List<Attachment__c>();
			//Creating Attachments__c object for inserting Attachments__c records based on file selected.	
			Attachment__c objAttach = new Attachment__c();
			
			if(strObjName=='Case')		//Attaching Attachments__c object from case Object
			objAttach.Case__c = strId;
			else
			objAttach.Case__c =null;
			
			
			if(strObjName=='Account')		//Attaching Attachments__c object from Account Object
			objAttach.Account__c  = strId;
			else
			objAttach.Account__c  =null;
			
			
			if(strObjName=='Lead')			//Attaching Attachments__c object from Lead Object
			objAttach.Lead__c  = strId;
			else
			objAttach.Lead__c =null;
			
			
			if(strObjName=='Contact')		//Attaching Attachments__c object from Contact Object
			objAttach.Contact__c  = strId;
			else
			objAttach.Contact__c  =null;
			
			
			objAttach.File_Type__c = strfileContentType;
			objAttach.File_Name__c = strFileName;
			objAttach.Name = strFileName;
			objAttach.Link_To_Download__c = strBaseUrl + '/apex/DisplayAmazonFile?BucName=' + strBucketName + '&FileName=' + strFileName;	
			lstAttachment.add(objAttach);
			
			//inserting lstAttachment list of  Attachment
			if(lstAttachment != null && lstAttachment.size() > 0)
				insert lstAttachment;  
			
			//	updating strAttachmentId with inserted object Attachment record 
			strAttachmentId = objAttach.Id;		
			
			// updating file name with Attachment__c.Id_File Name
			strFileName = objAttach.Id + '_' + strFileName ;
			
			//To cover the exception in the test
			/*if(Test.isRunningTest())
				integer i = 10/0;*/	 
    	}
    	catch(Exception e){
			//do nothing    		
    	}	
    }
}