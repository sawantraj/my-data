public with sharing class TB_CreateStudentToCourseController {
	
	public String searchText{get;set;}
	public string s;
	public Integer PageSize{get;set;}
	public Integer noOfPages{get;set;}
	public Integer PageNumber{get;set;}
	private Integer totalNoOfRec;
	private string searchlist{get;set;}
	
	public list<studentWrapper> studentList{get;set;}
	public list<Student__c> selectedStudents = new list<Student__c>();
	public list<Student_course__c> stucourse=new list<Student_course__c>();
	 public ApexPages.StandardSetController con{get; set;}  
	
	public class studentWrapper{
		
		public Student__c stu{get; set;}
		public Boolean isSelected{get; set;}
		public studentWrapper(Student__c s,Boolean v){
			stu = s;
			isSelected = v;
		}
	 }

	public TB_CreateStudentToCourseController(ApexPages.StandardController controller){
		studentList=new list<studentWrapper>();
		searchText=s;
		PageSize = 10;
        totalNoOfRec = [select count() from Student__c where Name like :searchText+'%'];
        system.debug('----------totalNoOfRec'+totalNoOfRec);
        getInitialAccountSet(); 
		
		}
		
		public PageReference getInitialAccountSet()
    	{
        PageNumber = 0;
        noOfPages = totalNoOfRec/PageSize;
        
        if (Math.mod(totalNoOfRec, PageSize) > 0)
            noOfPages++;
        try{
        	system.debug('---------comes under try------');
           selectedStudents = Database.query(searchlist+ 'limit'+PageSize);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
        return null;
   		 }
		
		public void runQuery(){
		Integer offset = pageNumber * pageSize;
		system.debug('--------value in offset-------'+offset);
        String query = searchlist + ' limit '+pageSize +' offset '+ offset;
        System.debug('Query is'+query);
        try{
            selectedStudents=Database.query(query);
            system.debug('-----------what in searchlist'+selectedStudents);	
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        } 
		}
	
	
	public pageReference doSearch(){
	 	studentList=new list<studentWrapper>();	
	 	if(studentList==null)
	 	studentList=new list<studentWrapper>();
	 	
	 	searchlist='select Name from Student__c where Name!=null ';
		searchlist += ' and Name LIKE \''+searchText+'%\'';
		system.debug('-----------what in searchlist'+searchlist);
		runQuery();
		
		for(Student__c obj :selectedStudents){
			studentWrapper stwr= new studentWrapper(obj,false);
			studentList.add(stwr);
		}
		    return null;
		  	}

	public PageReference onSave(){

		for(Student__c var:selectedStudents){
			Student_course__c stucourseobj=new Student_course__c();
			stucourseobj.Course__c=ApexPages.currentPage().getParameters().get('Id');
			stucourseobj.Student__c=var.Id;
			stucourse.add(stucourseobj);
			}
			system.debug('-------------stucourse---------'+stucourse);
		upsert stucourse;
		
		return null;
	 	}
	 	
	 
	 public PageReference onReset(){
	 	return null;
	 }
	 public PageReference onCancel(){
	 return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
	 }
	 
	 public pageReference GetSelected(){
	 	selectedStudents.clear();
	 	for(studentWrapper stuWrapper:studentList){
	 		if(stuWrapper.isSelected==true)
	 		selectedStudents.add(stuWrapper.stu);
	 		}
	 	return null;
	 	}
	 	// indicates whether there are more records after the current page set.
	 	
	 	 public PageReference next(){
	 	 	system.debug('come in next-------');
        PageNumber++;
        system.debug('come in pageNumber-------'+PageNumber);
        runQuery();
         system.debug('after in pageNumber-------');
        return null;
    }
	//set records to previous page
    public PageReference previous(){
        PageNumber--;
        if (PageNumber < 0)
            return null;
        runQuery();
        return null;
    }
	
}