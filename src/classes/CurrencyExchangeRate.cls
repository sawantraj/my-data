public with sharing class CurrencyExchangeRate {

    public Account acc1{get;set;}
    public Account acc2{get;set;}
    public String rate{get;set;}
    
    public void currencyRate(){
        Http h = new Http();
        String url='http://www.google.com/ig/calculator?hl=en&q=1';
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@  From '+acc1.CurrencyList__c);
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@  To '+acc2.CurrencyList__c);
        if(acc1.CurrencyList__c != null && acc2.CurrencyList__c != null)
            url = url +acc1.CurrencyList__c+'=?'+acc2.CurrencyList__c;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
            
        HttpResponse res = h.send(req);
        rate = res.getBody();
        system.debug('######################### '+res.getBody());
    }

}