public class dynamicPicklistController{
	
    public String src{get;set;} 
    public List<customField> fieldList { get; set; }
    public Integer para{get;set;}
    
    

    public class customField {
        List<SelectOption> dataTypes{get;set;} 
        Public Integer srNo{get;set;} 
        public String fieldType { get; set;}
        public String flagForDeletion { get; set; }
        private String Value;
        public String getTextValue() { return value; }
        public String getCheckValue() { return value; }
        public String getTextAreaValue() { return value; }
        public void setTextValue(String s) { value = s; }
        public void setCheckValue(String s) { value = s; }
        public void setTextAreaValue(String s) { value = s; }
        public Boolean getIsText() { return fieldType == 'text'; }
        public Boolean getIsCheck() { return fieldType == 'check'; }
        public Boolean getIsTextArea() { return fieldType == 'area'; }
        public List<String> lstPickString{get;set;}
        //public Integer para{get;set;}
        
        public customField(){
        lstPickString = new List<String>();
        lstPickString.add('abc');
        lstPickString.add('pqr');
        lstPickString.add('xyz');
   
        }
              
       /* public List<SelectOption> getDataTypeList() {
            customField cf = new customField();
            dataTypes = new List<SelectOption>();
            for(String objPick:cf.lstPickString){
           
            dataTypes.add(new SelectOption(objPick,objPick));
           
          
            
            }
            return dataTypes; 
        }*/
        
    }

    public List<SelectOption> getDataTypeList(){

   if(fieldList.size()==1){
   	 //system.debug('-------para values-----'+ApexPages.currentPage().getParameters().get('para'));
     system.debug('-------fieldList[0].dataTypes------'+fieldList[0].dataTypes);
    return fieldList[0].dataTypes;
    }
    else{
    	system.debug('-------fieldList[1].dataTypes------'+fieldList[1].dataTypes);
    return fieldList[1].dataTypes;
    
    
    } 
    }
    //return null;
 	
 	public dynamicPicklistController() {
        fieldList = new List<customField>();
		para=0;
          customField cf = new customField();
          cf.srNo = 0; 
            cf.fieldType = 'text';
            cf.flagForDeletion = 'false';
            cf.setTextValue('');
             cf.dataTypes = new List<SelectOption>();
            for(String objPick:cf.lstPickString){
           system.debug('---------------objPick------'+objPick);
            cf.dataTypes.add(new SelectOption(objPick,objPick));
            }
            fieldList.add(cf);
            //system.debug('-------para values-----'+ApexPages.currentPage().getParameters().get('para'));
	}
    
     
    public void addRow() {
        system.debug('--------------fieldList'+fieldList);
        
        customField cf = new customField();
       
        List<SelectOption> dataTypes1 = new List<SelectOption>();
        cf.flagForDeletion = 'false';
        cf.setTextValue('');
        
        
         for(String objPick :cf.lstPickString){
         if(!fieldList.isEmpty()){
            if(fieldList[0].fieldType!=objPick){
            	system.debug('---------------objPick------'+objPick);
               dataTypes1.add(new SelectOption(objPick,objPick));
              system.debug('--------------true');            
            }
           }             
         }
         cf.dataTypes = dataTypes1; 
        fieldList.add(cf);
        system.debug('--------------fieldList ==end'+fieldList);
        system.debug('-------para values-----'+para);

    }
    
    public void delRows() {
        for(Integer i = fieldList.size()-1; i>=0; i--) {
            if(fieldList[i].FlagForDeletion == 'true') {
                fieldList.remove(i);
            }
        }
    }
	
}