public with sharing class CampaignCustomAccountLookupController {
	
  Public Campaign camp {get;set;} // new account to create
  Public Id contactId;
  Public List<Campaign> results {get;set;} // search results
  Public List<Id> campLst {get;set;}
  Public Set<Id> conSet;
  Public Map<Id,Contact> conMap;
  Public string searchString {get;set;} // search keyword
  
 
  Public CampaignCustomAccountLookupController() {
    camp = new Campaign();
   
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('ContactID');
    
    conMap = new Map<Id,Contact>([select Id from Contact where Name =: searchString]);
    for(Contact c: conMap.values()){
    	contactId = c.Id;
    }
    conset = conMap.keyset();
    runSearch();  
  }
  
  // prepare the query and issue the search command
  Private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  Private List<Campaign> performSearch(string searchString) {
 	String soql = 'select id, name from Campaign where Contact__c IN ';
    if(searchString != '' && searchString != null)
      soql +=':conset';
   	System.debug(soql);
    return database.query(soql); 
 
  }
  
  Public PageReference saveCampaign(){
  	system.debug('inside saveCampaign');
  	camp.Contact__c = contactId;
  	camp.Type = 'Parent';
  	insert camp;
  	//PageReference campPage = new ApexPages.StandardController(camp).view();
  	PageReference campPage = page.CampaignNewRecord;
  	campPage.getParameters().put('id', camp.id);
    campPage.setRedirect(true);
    return campPage;
  } 
 
 
}//CampaignCustomAccountLookupController ends