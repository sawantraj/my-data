public with sharing class TestCheckboxEmployeeController 
{
	public Employee__c[] employee{get;set;}
	public Salary_Details__c[] employeeinformation{get;set;}
	set<Id> recordId=new set<Id>();
	map<String,RecordType> maprecordType=new map<String,RecordType>();
	
	
	public TestCheckboxEmployeeController(ApexPages.StandardController controller)
	{
	Id value=controller.getRecord().Id;
	system.debug('@@@@@@@@@@@@@@=========value'+value);
	employee=[select Id from Employee__c where Id=:controller.getRecord().Id limit 1];
	system.debug('@@@@@@@@@@@@@@=========employee'+employee);
			
	}
}