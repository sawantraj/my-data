global class scheduledAcoountStatusUpdate implements Database.Batchable<sobject>
{
   global final String Query;
   global final String Entity;
   global final String Field;
   global final String Value;


   global scheduledAcoountStatusUpdate(String q)
   {
             Query=q;
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }
   
   global void execute(Database.BatchableContext BC,
                       List<sObject> scope)
   {
      List<Account> updateAccts = new List<Account>();
      for(Sobject s : scope)
      {
          Account a = (Account) s;
          a.Name = a.Name + 'Batch 1.';
      }     
      update updateAccts;
   }
   
   //The batch process has completed successfully. Schedule next batch.   
   global void finish(Database.BatchableContext BC)
   {
        System.debug(LoggingLevel.WARN,'Batch Process 1 Finished');
        
        //Build the system time of now + 20 seconds to schedule the batch apex.
        Datetime sysTime = System.now();
        sysTime = sysTime.addSeconds(20);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        system.debug(chron_exp);
        
        AccountScheduled acctBatch2Sched = new AccountScheduled();
        
        //Schedule the next job, and give it the system time so name is unique
        System.schedule('acctBatch2Job' + sysTime.getTime(),chron_exp,acctBatch2Sched);
   }
}