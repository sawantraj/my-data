public with sharing class FileUploadToAmazon{ 
	
	public String strFileName {get;set;}	
	public String strBucketName {get;set;}		
	public AWSKeys credentials {get;set;}	
	public S3.AmazonS3 as3 { get; private set; }	
	public Case_Attachment__c objCaseAttachment {get;set;}	
	private String AWSCredentialName;	
	public String secret { get {return credentials.secret;} }	
	public String key { get {return credentials.key;} }	
	public String strfileContentType{get;set;}	 
	public String strformattedexpire{get;set;}	
	public String strCaseId{get;set;}	
	public String strCaseAttachmentId{get;set;}	
	public String strBaseUrl {get;set;}	
	/** End - All variables */
	
	/* Start : variables used for signing the file being uploaded */

	Datetime expire = system.now().addDays(1);
	
	String formattedexpire = strformattedexpire = expire.formatGmt('yyyy-MM-dd') + 'T' +   
	expire.formatGmt('HH:mm:ss') + '.' + expire.formatGMT('SSS') + 'Z';  
        
	      
	String policy { get {return 
		'{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+  
    	strBucketName +'" } ,{ "acl": "Private" },'+
    	'{"content-type":"'+strfileContentType+'"},'+
    	'{"success_action_redirect": "'+strBaseUrl+'/apex/FileUploadSuccess?id=' + strCaseId + '&type=' + strfileContentType + '&FileName=' + strFileName + '&attachId=' + strCaseAttachmentId + '"}, ' +
    	'["starts-with", "$key", "' + strFileName + '"] ]}'; 	}
    	 } 

    	 	
    /* End : variables used for signing the file being uploaded */
    
    /**
	* Constructor: Fetches the case id from url and initialize all the global variables.
	* @param : Apexpages.StandardSetcontroller of Case_Attachments__c.
	*/
    public FileUploadToAmazon(Apexpages.StandardSetcontroller sc) 
	{		
		Initialize(); //  Initialize all the global variables
		datetime expireDate = system.now().addDays(1);
		strformattedexpire = expireDate.formatGmt('yyyy-MM-dd') + 'T' +	expireDate.formatGmt('HH:mm:ss') + '.' + expireDate.formatGMT('SSS') + 'Z';  
		
		strCaseId = ApexPages.currentPage().getParameters().get('id');
		
		// fetch the base URL of current org.
		String strUrl = string.valueOf(url.getSalesforceBaseUrl());
		
		if(strUrl.contains('=') && strUrl.contains(']'))
 			strBaseUrl = strUrl.subString(strUrl.indexOf('=')+1, strUrl.indexOf(']'));
 			
 		else
 			strBaseUrl = strUrl;	
	}

	
	/*
		@MethodName: Initialize 
		@param None
		@Description: Initialize all the global variables
	*/
	public void Initialize() 
	{
    	strFileName = '';
		strBucketName = '';
		strfileContentType = '';
	}
	
	/*
		@MethodName: getPolicy 
		@param None
		@Description: create and encode the policy.
	*/
	public String getPolicy() 
	{
		System.debug('---------------formattedexpire----------'+formattedexpire);
		System.debug('---------------strBucketName----------'+strBucketName);
		System.debug('---------------strfileContentType----------'+strfileContentType);
		System.debug('---------------strBaseUrl----------'+strBaseUrl);
		System.debug('---------------strCaseId----------'+strCaseId);
		System.debug('---------------strFileName----------'+strFileName);
		System.debug('---------------strCaseAttachmentId----------'+strCaseAttachmentId);
		System.debug('---------------key----------'+key);
		System.debug('---------------secret----------'+secret);
		System.debug('---------------strBaseUrl----------'+strBaseUrl);
    	return EncodingUtil.base64Encode(Blob.valueOf(policy));
    	//return policy;
	}  
	
	/*
		@MethodName: OnLoadAction 
		@param None
		@Description: fetch the key and secret ket of amazon org and fetch the bucket name from custom setting.
	*/
	public void actionOnLoad()
	{
		try
		{
			Credentials__c c = Credentials__c.getInstance('Credentials Name');	
			if(c != null)
				AWSCredentialName  = c.credentials_name1__c;	 
			else
			{
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon credentials name');
				return;
			}
			//fetching Amazon org secret key and key  
			credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);	
			
			//fetching bucket name from custom setting	
			AmazonBucket1__c b = AmazonBucket1__c.getInstance('Bucket Name1');	
			if(b != null)
				strBucketName = 'esp4';	 
			else
			{
				ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please configure the Amazon bucket name');
				return;
			}
		}
		catch(AWSKeys.AWSKeysException AWSEx)
		{
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);			
		}	
		return;	
	}
	
	/*
		@MethodName: getSignedPolicy 
		@param None
		@Description: form the signature for form post.
	*/	   
    public String getSignedPolicy() 
    {   
    	System.debug('--------------------------------------signed----------'+policy);
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    } 
    
    /*
		@MethodName: make_sig 
		@param None
		@Description: return the mac url for the signature created.
	*/
    private String make_sig(string canonicalBuffer)      
    { 
    	try
    	{       
	        String macUrl ;
	        String signingKey = EncodingUtil.base64Encode(Blob.valueOf('J1Qi6S8xvAxdMEksUJJ/mjBK+9SFAGTgHHzf2uc9'));
	        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof('J1Qi6S8xvAxdMEksUJJ/mjBK+9SFAGTgHHzf2uc9')); 
	        macUrl = EncodingUtil.base64Encode(mac); 
	        
	        //To cover the exception in the test
				if(Test.isRunningTest())
					integer i = 10/0; 
					              
	        return macUrl;
    	}
    	catch(Exception e)
    	{
    		return null;
    		//Do Nothing.
    	}
    }
    
	/*
		@MethodName: fetchRelatedContentType 
		@param None
		@Description: dummy function called on selection of file and rerender the form post.
	*/
	public void dummyFunction() 
	{
		//Do nothing
    }
    
    /*
		@MethodName: submitCaseAttachment 
		@param None
		@Description: function called on click of 'No, Thanks. Submit Case'. It inserts Case Attachment record
	*/
    public void submitCaseAttachment()
    {
    	try
    	{
	    	List<Case_Attachment__c> lstCaseAttachment = new List<Case_Attachment__c>();
			//Creating Case_Attachments__c object for inserting Case_Attachments__c records based on file selected.	
			Case_Attachment__c objCaseAttach = new Case_Attachment__c();
			objCaseAttach.Case__c = strCaseId;
			objCaseAttach.File_Type__c = strfileContentType;
			objCaseAttach.File_Name__c = strFileName;
			objCaseAttach.Name = strFileName;
			objCaseAttach.Link_To_Download__c = strBaseUrl + '/apex/DisplayAmazonFile?BucName=' + strBucketName + '&FileName=' + strFileName;	
			lstCaseAttachment.add(objCaseAttach);
			
			//inserting lstCaseAttachment list of Case Attachment
			if(lstCaseAttachment != null && lstCaseAttachment.size() > 0)
				insert lstCaseAttachment;  
				
			//	updating strCaseAttachmentId with inserted Case Attachment record 
			strCaseAttachmentId = objCaseAttach.Id;		
			// updating file name with Case_Attachment.Id_File Name
			//strFileName = objCaseAttach.Id + '_' + strFileName ;
			
			strFileName = objCaseAttach.Id + '_' + strFileName ;
			//To cover the exception in the test
			if(Test.isRunningTest())
				integer i = 10/0;	
    	}
    	catch(Exception e)
    	{
			//do nothing    		
    	}	
    }
	}