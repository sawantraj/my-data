public with sharing class InsertContactNameToAccount {
	
	
	
	Public Static Integer afterInsertContact(Map<Id,Contact> pConMap){
		Set<Id> conSet = new Set<Id>();
		Set<ID> accSet = new Set<Id>();
		Map <String,Contact> conValuesMap = new Map <String,Contact>();
		Map <String,Account> accValuesMap = new Map <String,Account>();
		List<Account> accLst = new List<Account>();
		
		for(Contact conAccID:pConMap.Values()){
			accSet.add(conAccID.AccountId);
			conSet.add(conAccID.Id);
		}
		
		for(Contact con :[select Id,FirstName,AccountId
						  from Contact 
						  Where Id IN :conSet]){
		  	
		  	conValuesMap.put(con.FirstName,con);
						  	
	  	}
	  	
	  	for(Account acc : [Select Id,contact_name__c 
	  					  from Account 
	  					  where Id IN :accSet]){
	  		accValuesMap.put(acc.contact_name__c,acc);				  	
  		}
	  	
	  	try{
		  	for(Contact con :conValuesMap.values()){
		  	
		  		for(Account acc :accValuesMap.values()){
		  					  	
	  					if(acc.Id == con.AccountId){
	  						if((acc.contact_name__c != Null) &&(!accValuesMap.containsKey(con.FirstName)))
	  							acc.contact_name__c = acc.contact_name__c + ',' + con.FirstName ;
	  						else
							acc.contact_name__c = con.FirstName;
						}
		  			update acc;
	  				
	  			}// second for	  	
		  	}
  		
	  	}	
	  	catch(exception e){
	  		
	  	}
		return -1;
	}//afterInsertContact ends
	
	Public Static Integer afterDeleteContact(Map<Id,Contact> pConDeleteMap){
		
		String ContactName = ' ';
		string deletedConatct = ' ';
		String strContact = '';
		Map<String,Account> modifiedConMap = new Map<String,Account>();
		Set<ID> accSet = new Set<Id>();
		Set<String> modifiedConset = new Set<String>();
		List<String> modifiedConLst;
		
		for(Contact conAccId:pConDeleteMap.Values()){
			accSet.add(conAccId.AccountId);
			deletedConatct = conAccId.FirstName;
		}
			
		for(Account acc : [Select Id,contact_name__c from Account where Id IN :accSet]){
			modifiedConMap.put(acc.contact_name__c, acc); 	
		}
		try{
	  		
		  	for(Account acc1 :modifiedConMap.values()){
	  			ContactName = acc1.contact_name__c;
	  			modifiedConLst = ContactName.split(',');
	  			modifiedConset.addAll(modifiedConLst);
	  			
			  	if(modifiedConset.contains(deletedConatct)){
			  		modifiedConset.remove(deletedConatct);
		  			for(String str :modifiedConset){
		  				if(strContact == '')
						strContact = str;
						else
						strContact += ','+str;
		  			}
		  			acc1.contact_name__c = strContact;
		  			update acc1;
			  	}
		  	}
		}
	  	catch(Exception e){}
		return -1;
	}//afterDeleteContact ends
	
	Public Static Integer afterUpdateContact(Map<Id,Contact> pConUpdateMap,Map<Id,Contact> pConBefUpdateMap){
		Id ConId;
		String ContactName = ' ';
		String strContact = '';
		String updatedContactName = '';
		String oldContactName = '';
		String newContactName = '';
		List<String> modifiedConLst;
		Set<Id> accSet = new Set<Id>();
		Set<Id> conSet = new Set<Id>();
		Set<String> modifiedConset = new Set<String>();
		Map<Id,String> oldContactMap = new Map<Id,String>();
		Map<Id,String> NewContactMap = new Map<Id,String>();
		
		for(Contact cc : pConBefUpdateMap.values()){
			accSet.add(cc.AccountId);
			conSet.add(cc.Id);
			oldContactMap.put(cc.Id,cc.FirstName);	
		}
		
		for(Contact ccc :[select Id,FirstName from Contact where Id IN :conSet]){
			NewContactMap.put(ccc.Id,ccc.FirstName); 
			conId = ccc.Id;
		}
		
		try{
			for(Account acc : [select Id,contact_name__c from Account where Id IN :accSet]){
				ContactName = acc.contact_name__c;
		  		modifiedConLst = ContactName.split(',');
		  		modifiedConset.addAll(modifiedConLst);
		  		oldContactName = oldContactMap.get(conId);
		  		newContactName = NewContactMap.get(conId);
		  		
		  		for(string str : modifiedConset){
		  			if(str.equalsIgnoreCase(oldContactName))
		  				strContact = str.replace(oldContactName,newContactName);
		  			
		  			if(updatedContactName == ''){
		  				if(strContact == '')
		  				updatedContactName = str;
		  				else
		  				updatedContactName = strContact;
		  			}
		  			else{
		  				if(strContact == '')
		  				 	updatedContactName += ','+ str;
		  				else{
		  					if(updatedContactName.contains(strContact))
		  					updatedContactName += ','+ str;
		  					else
		  					updatedContactName += ','+strContact;
		  				}//second else
		  			}
		  		}// inside for
	  				acc.contact_name__c = updatedContactName;
		  			update acc;
		  			
		  	}
		  	modifiedConLst.clear();
		}//try
		catch(Exception e){}
		return -1;
	}//afterUpdateContact ends

}