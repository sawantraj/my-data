public with sharing class NewProductControllerClass2 
{
		public Product2 productobj{get;set;}
		public List<Product2> products1 {get; set;}
		public List<Actions> inneractions { get; set; }
		
		
	public NewProductControllerClass2(ApexPages.StandardController controller)	//first constructor
	{
	 products1=new List<Product2>();
	products1.add(new Product2());
	getdata();
	}
	
	public void getdata()
	{
		inneractions.add( new Actions());
	}

		public void addrow()
		{
		
		products1.add(new Product2());
		}
	
		public void removeRow()
     	{ 
     	
		Integer i = products1.size();
 		products1.remove(i-1);
 		}
 		
 		
 		 public class Actions
	{
        public String Id {get; set;}
         public String Name {get; set;}
        public String  ProductCode{get; set;}
        public Boolean IsActive{get; set;}
        public boolean checked{get;set;}
        
        public void actionsmethod() 
        {
            if(checked)
            {
            system.debug('@@@@@@@@@@@@@@===============>getting Id'+Id);
            
            product2 p=[select ProductCode, Name, IsActive From Product2 where Id=:Id limit 1];
            delete p;
            }
        }
        
    }
	
	
	public PageReference save() 
	{
		 insert products1;       
		 PageReference home = new PageReference('/home/home.jsp');       
		home.setRedirect(true);       
		 return home;
	}
}