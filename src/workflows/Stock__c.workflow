<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Base_Quanttiy_Received_update</fullName>
        <field>Base_Quantity_recieved__c</field>
        <formula>Quantity_received__c</formula>
        <name>Base Quanttiy Received update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copied quantity received</fullName>
        <actions>
            <name>Base_Quanttiy_Received_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Stock__c.Quantity_received__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>This workflow will fire when stock created and copy value of quantity received in base quantity Received.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
