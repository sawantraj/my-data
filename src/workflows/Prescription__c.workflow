<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Checkbox_turns_to_true</fullName>
        <field>Status_become_partially_collected__c</field>
        <literalValue>1</literalValue>
        <name>Checkbox turns to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Drug__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>collected_status_comes_true</fullName>
        <field>Status_changed_to_collected__c</field>
        <literalValue>1</literalValue>
        <name>collected status comes true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Drug__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Collected checkbox turn to true</fullName>
        <actions>
            <name>collected_status_comes_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Prescription__c.Status__c</field>
            <operation>equals</operation>
            <value>cancelled,Approval declined,Collected</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Partially collected Checkbox upadate to true</fullName>
        <actions>
            <name>Checkbox_turns_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Prescription__c.Status__c</field>
            <operation>equals</operation>
            <value>Partially collected</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
