<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>salary_rule</fullName>
        <field>Is_Taxaable__c</field>
        <literalValue>1</literalValue>
        <name>salary rule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Salary credit rule</fullName>
        <actions>
            <name>salary_rule</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Employee__c.Salary_Credited_Till_Date__c</field>
            <operation>greaterThan</operation>
            <value>12000</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
