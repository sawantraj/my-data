<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>email_alert</fullName>
        <description>email alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email_to_send_Alert__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CustomerPortalChangePwdEmail</template>
    </alerts>
    <fieldUpdates>
        <fullName>field_update</fullName>
        <field>Email_to_send_Alert__c</field>
        <formula>Name +&apos;@gmail.com&apos;</formula>
        <name>field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_update_to_overdue</fullName>
        <field>Status__c</field>
        <literalValue>overdue</literalValue>
        <name>status update to overdue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Appointment_done</fullName>
        <field>Appointment_Done__c</field>
        <formula>&quot;TRUE&quot;</formula>
        <name>update Appointment done</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Copied Email Address to email</fullName>
        <actions>
            <name>field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Appointment__c.IsChecked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Appointment time</fullName>
        <active>true</active>
        <formula>AppointmentStatusUpdate__c &lt; Start_Date__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>update_Appointment_done</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Appointment__c.AppointmentStatusUpdate__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Appointment to overdue</fullName>
        <actions>
            <name>status_update_to_overdue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOW() &gt; Start_Date__c , 
ISPICKVAL(  Status__c  , &quot;Booked&quot;),  
Appointment_Done__c  = &quot;TRUE&quot;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
